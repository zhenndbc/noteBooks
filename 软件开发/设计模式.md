
## 代理模式

> 通过代理模式去增强或者控制原有对象的一些操作
```go
type (
	OrginObject struct {
		action string
	}
	ObjectSame interface {
		run(string) error
	}
	ProxyObject struct {
		Name string
		object *OrginObject
	}
)

func (ob OrginObject) run(a string) error {
	fmt.Printf("Run %s", a)
	return nil
}

func (ob ProxyObject) run(a string) error {
	if ob.Object == nil{
		ob.Object = new(Object)
	}
	if a == "run" {
		ob.Object.run(a)
	}
}
```

```go
/*

    proxy pattern provide object control access to another object, intercepting all calls

    //设计思想

        1. 代理inteface

        2. 真实对象Object struct

        3. 代理对象ProxyObject struct，属性为Object, 拦截所有的action

        4. 方法ObjDo处理所有的逻辑

*/


//1.use proxy and object they must implement same methods
type IObject interface {

    ObjDo(action string)

}

//2.object represents real objects which proxy will delegate data
type Object struct {

    action string

}

//3.ObjDo implement IObject interface and handle all logic
func (obj *Object) ObjDo(action string) {

    fmt.Printf("I can %s", action)

}

//ProxyObject represent proxy object with intercepts actions
type ProxyObject struct {

    object *Object

}

//拦截作用
func (p *ProxyObject) ObjDo(action string) {

    if p.object == nil {

        p.object = new(Object)

    }

    if action == "run" {

        p.object.ObjDo(action)

    }

}

/*

    same interface and function to sturct

    represent *object => change orgin object function or add some other method.

*/
```