## 同源策略
同源就是域名、协议、端口完全相同
ajax请求只能同源

## CORS概述
跨域资源共享(Cross-Origin Resource Share 简称CORS)
浏览器向跨源服务器发出XMLHttpRequest请求

CORS允许服务器声明哪些源站通过浏览器有权限访问哪些资源，并且对于那些对服务器可能产生副作用的HTTP请求，例如POST、DELETE请求等，浏览器必须首先使用OPTIONS方法发起一个预检请求(prelight request)，从而获知服务器是否允许该跨域请求。

OPTION 是为了获取服务器是否运行该跨域请求而设定的预检请求

## CORS实现机制

CORS标准新增了一组HTTP头字段来实现该机制。服务端通过设置相应的头部字段来控制是否允许该次跨域请求，如果服务端没有返回正确的响应头部，则请求方不会收到任何数据。

## Gin 实现 CORS
```go
func Cors() gin.HandlerFunc {
   return func(c *gin.Context) {
      method := c.Request.Method
      origin := c.Request.Header.Get("Origin") //请求头部
      if origin != "" {
         // 当Access-Control-Allow-Credentials为true时，将*替换为指定的域名
         c.Header("Access-Control-Allow-Origin", "http://a.com") 
         c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
         c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Extra-Header, Content-Type, Accept, Authorization")
         c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
         c.Header("Access-Control-Allow-Credentials", "true")
         c.Header("Access-Control-Max-Age", "86400") // 可选
         c.Set("content-type", "application/json") // 可选
      }

      if method == "OPTIONS" {
         c.AbortWithStatus(http.StatusNoContent)
      }

      c.Next()
   }
}


func main() {
  r := gin.Default()
  r.Use(Cors()) //开启中间件 允许使用跨域请求
  // 其他路由设置
  r.run()
}
```