错误信息：
> TERM environment variable not set.
截屏：
![[Pasted image 20231205013814.png]]
```shell
echo "export TERM=xterm" >> ~/.bashrc && source ~/.bashrc
```
---

`一行代码修改debian源`
```shell
sudo sed -i 's/deb.debian.org/mirrors.aliyun.com/g' /etc/apt/sources.list
```
`fdisk`
```shell
# 安装依赖
sudo apt-get update
sudo apt-get install parted

# 查看已有分区信息
sudo parted -l
```
---
## Linux 挂载分区
要将U盘挂载到你的Debian系统中以扩展内存，你可以按照以下步骤进行操作：

1. 插入U盘：将30GB的U盘插入到你的WiFi棒上。

2. 查看设备：首先，你需要查看U盘在系统中的设备名称。你可以使用以下命令来列出已连接的设备：

```
sudo fdisk -l
```

通常，U盘会被分配一个设备名称，例如`/dev/sdX`，其中X是字母（例如，/dev/sdb）。

3. 创建挂载点：在将U盘挂载到系统之前，你需要创建一个挂载点。可以选择一个已存在的目录，或者创建一个新的目录。例如，我们将创建一个名为`/mnt/usb`的新挂载点：

```
sudo mkdir /mnt/usb
```

4. 挂载U盘：使用以下命令将U盘挂载到刚刚创建的挂载点：

```
sudo mount /dev/sdX /mnt/usb
```

确保将`/dev/sdX` 替换为你在第2步中找到的U盘设备名称。

5. 查看挂载情况：你可以使用以下命令查看U盘是否已成功挂载：

```
df -h
```

6. 使用挂载的U盘：现在，U盘已挂载到系统中，你可以将文件保存到U盘上，以扩展系统的可用存储空间。请注意，如果你想在每次启动时都自动挂载U盘，你可以将其添加到`/etc/fstab`文件中，以便持久挂载。

7. 卸载U盘：在不再需要挂载的U盘时，你可以使用以下命令将其卸载：

```
sudo umount /mnt/usb
```

这些步骤将帮助你将U盘挂载到Debian系统中，以扩展内存和存储空间。

---

## Debian 版本

1. 使用 `lsb_release` 命令：

```bash
lsb_release -a
```

这个命令会显示你的Debian版本信息，包括发行版号码、发行代号、描述等。

2. 查看 `/etc/os-release` 文件：

```bash
cat /etc/os-release
```

在该文件中，你会找到包含 Debian 版本信息的字段，如 `VERSION_ID`。

3. 查看 `/etc/debian_version` 文件：

```bash
cat /etc/debian_version
```

这将显示 Debian 版本的简单版本号。

通过上述方法之一，你可以方便地确定你的 Debian 版本。

---
## 配置初始 Debian 源

如果在更新软件包时出现了问题，无法通过安装`ca-certificates`解决，你可以尝试以下替代方式，通过使用HTTP而不是HTTPS来更新软件包。请注意，这会降低安全性，因为不再使用加密的HTTPS连接。

1. 打开你的`/etc/apt/sources.list`文件进行编辑，可以使用`echo`命令或任何可用的文本编辑器，例如`cat`命令：

```bash
sudo cat > /etc/apt/sources.list << EOF
deb http://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free
deb-src http://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free
deb http://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free
deb-src http://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free
deb http://mirrors.tuna.tsinghua.edu.cn/debian-security bullseye-security main contrib non-free
deb-src http://mirrors.tuna.tsinghua.edu.cn/debian-security bullseye-security main contrib non-free
EOF
```

这会将软件源更改为使用HTTP而不是HTTPS。

2. 保存文件后，运行以下命令以更新软件包列表：

```bash
sudo apt update
```

这样，你将使用HTTP连接来获取更新，而不再需要证书。

请注意，虽然这个方法可能解决了证书问题，但它将导致连接不再受到加密保护，因此在网络传输期间存在潜在的安全风险。如果安全性对你来说很重要，你应该尽快解决证书问题，以恢复HTTPS连接。

```shell
sudo apt clean
cd /var/lib/apt 
sudo mv lists lists.old
sudo mkdir -p lists/partial
sudo apt update 
```

---

要清除系统中的不需要的多余文件，可以使用以下命令：

1. **清除已安装但不再需要的软件包：**

   使用以下命令清理已安装但不再需要的软件包（被称为孤立的软件包）：

   ```bash
   sudo apt autoremove
   ```

   这将删除已安装的软件包中不再被其他软件依赖的软件包。

2. **清理软件包缓存：**

   你可以清理已下载但不再需要的软件包文件，以释放磁盘空间：

   ```bash
   sudo apt clean
   ```

   这将删除软件包的下载缓存，但保留已安装软件包的备份。

3. **清理旧的配置文件：**

   如果你曾经删除了某个软件包，但它的配置文件仍然存在，你可以使用以下命令清理旧的配置文件：

   ```bash
   sudo dpkg --purge $(dpkg -l | grep '^rc' | awk '{print $2}')
   ```

   这将删除所有标记为 "rc"（已删除但仍保留配置文件）的软件包。

4. **清理临时文件和缓存：**

   使用以下命令来清理临时文件和缓存：

   ```bash
   sudo apt-get clean
   ```

   这会删除系统中的临时文件和缓存，释放磁盘空间。

5. **清理系统日志文件：**

   你可以清理系统日志文件以释放磁盘空间。通常，日志文件存储在 `/var/log/` 目录下，你可以使用以下命令清理它们：

   ```bash
   sudo rm /var/log/*.log
   ```

   请小心使用这个命令，确保你只删除不再需要的日志文件。

注意：在执行这些命令之前，请确保你知道自己在做什么，不要删除重要的文件。如果你不确定某个文件是否可以删除，最好进行备份或咨询相关的文档。

---
了解了你的需求，你可以创建一个包含 Go 和 React 的容器，用于制作一个面板来操作 Podman，并运行你的 Go 程序。以下是一些步骤和建议：

**1. 创建容器：**

首先，你需要创建一个容器，其中包含 Go 和 React，以及一个 Web 服务器来托管 React 前端。你可以使用 Dockerfile（如果你选择使用 Docker）或 Podman 配置文件来定义容器的构建过程。在容器中，确保 Go 和 React 工具已经安装。

这是一个简化的 Dockerfile 示例：

```Dockerfile
# 使用 Golang 镜像作为基础镜像
FROM golang:1.16

# 安装 React 所需的工具（例如，Node.js 和 npm）
RUN apt-get update && apt-get install -y nodejs npm

# 设置工作目录
WORKDIR /app

# 复制 Go 和 React 代码到容器中
COPY go-code /app/go-code
COPY react-code /app/react-code

# 构建 React 前端
WORKDIR /app/react-code
RUN npm install
RUN npm run build

# 设置工作目录为 Go 代码
WORKDIR /app/go-code

# 启动 Web 服务器
CMD ["go", "run", "main.go"]
```

**2. 构建容器镜像：**

使用以下命令来构建容器镜像：

```bash
docker build -t my-go-react-app-image .
```

或者

```bash
podman build -t my-go-react-app-image .
```

这将构建一个容器镜像，其中包含了 Go 和 React，并设置了一个命令来启动 Web 服务器以托管 React 前端。

**3. 运行容器：**

你可以使用以下命令来运行容器：

```bash
docker run -d --name my-go-react-app-container -p 8080:8080 my-go-react-app-image
```

或者

```bash
podman run -d --name my-go-react-app-container -p 8080:8080 my-go-react-app-image
```

这将在后台运行容器，并将容器的 8080 端口映射到主机的 8080 端口，以便你可以通过 Web 浏览器访问 React 前端。

**4. 创建上传文件的接口：**

在你的 Go 代码中，创建一个接口来处理文件上传。这个接口可以接收上传的文件，并将它们保存在容器内的指定目录中。

**5. 创建操作 Podman 的接口：**

在你的 Go 代码中，创建一个接口来操作 Podman。你可以使用 Podman 命令行工具（例如 `podman run`）来运行新的 Go 程序容器，并通过 API 返回结果。

**6. 创建 React 前端：**

使用 React 创建一个前端界面，该界面允许用户上传文件和触发操作 Podman 的请求。你可以使用 Axios 或其他 HTTP 客户端库来与 Go 后端通信。

**7. 启动容器并访问面板：**

启动容器后，通过浏览器访问容器的 IP 地址和端口（例如 `http://localhost:8080`），以访问 React 前端面板。在面板上，你可以上传文件并触发操作 Podman 的请求。

通过这些步骤，你可以创建一个包含 Go 和 React 的容器，用于制作一个面板来操作 Podman 并运行你的 Go 程序。这样，你可以轻松地上传文件和管理容器，同时保持容器的占用空间小和安全性。

---
