# Android 开发

> 1. 声明语句
>
>    <?xml version="1.0" encoding="utf-8"?>
>
> 2. 属性值必须使用引号  `" "`
>
> 3. 标签是成对出现的 `< />`

## 布局 1（LinearLayout）

> 水平或者垂直 从左到右 从上到下 每一列中只允许有一个子视图和控件

---

属性

![属性1](C:\Users\16143\Desktop\Android开发\02-布局管理\1-LinearLayout\属性1.png)

---

方向

![one](C:\Users\16143\Desktop\Android开发\02-布局管理\1-LinearLayout\one.png)

---

权重（比例）

![权重](C:\Users\16143\Desktop\Android开发\02-布局管理\1-LinearLayout\权重.png)

---

**注意**

![注意](C:\Users\16143\Desktop\Android开发\02-布局管理\1-LinearLayout\注意.png)

---

