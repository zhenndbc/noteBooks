# 我是一级标题

> 下列教程：如何使用markdown语法  
>
>   Markdown对于知识输出的作用不言而喻，学习掌握Markdown语法，
>
> 能够让我们专注于代码本身。Markdown的可移植性是亮点。

---

##  Markdown 标题语法

​	使用 # 号能够渲染为标题，标题分为六级：

> ```markdown
> # 一级标题
> ## 二级标题
> ### 三级标题
> #### 四级标题
> ##### 五级标题
> ###### 六级标题
> ```

---

## Markdown段落语法

​	要创建段落，请使用空白行将一行或多行文本进行分隔。

> ```markdown
> # 使用<br>可以达到换行效果
> <br>
> # 使用 空白行 用来达到换行效果
> ```

---

## Markdown强调语法

> ```markdown
> # 粗体（Bold）
> 要加粗文本，请在单词或短语的前后各添加两个星号（asterisks）或下划线（underscores）。如需加粗一个单词或短语的中间部分用以表示强调的话，请在要加粗部分的两侧各添加两个星号（asterisks）。
> ```

> 看看这两段有什么不同：  __中间加粗__  还有这个 ：__我是一个小天才__

---

## Markdown引用语法

> ```markdown
> 使用 > 可以创建引用
> 
> > Dorothy followed her through many of the beautiful rooms in her castle.
> >
> > The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.
> ```
>
> ##### 多个的块状引用
>
> > Dorothy followed her through many of the beautiful rooms in her castle.
> >
> > The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.

>##### 带有其他元素的
>
>> #### The quarterly results look great!
>>
>> - Revenue was off the chart.
>> - Profits were higher than ever.
>>
>>  *Everything* is going according to **plan**.

---

## Markdown的列表语法

> ##### 有序列表
>
> ```markdown
> 每个列表项前添加数字并紧跟一个英文句点。数字不必按数学顺序排列(1开始)
> 1. 我是一号
> 2. 我是二号
> 3. 我是三号
> 
> ###### 嵌套列表
> 1. First item
> 2. Second item
> 3. Third item
>     1. Indented item
>     2. Indented item
> 4. Fourth item
> ```
>
> ##### 无序列表
>
> ```markdown
> 每个列表项前面添加破折号 (-)、星号 (*) 或加号 (+) 
> ###### 正常无序列表
> - First item
> - Second item
> - Third item
>     - Indented item
>     - Indented item
> - Fourth item
> ```
>
> ##### 嵌套其他元素
>
> * ​    你好
>
> * ​    我也好
>
> * ​    大家都好
>
>   ```markdown
>   *    你好
>   *    我也好
>   ```
>
>   ###### 引用块
>
>   ```markdown
>   *   This is the first list item.
>   *   Here's the second list item.
>   
>       > A blockquote would look great below the second list item.
>   
>   *   And here's the third list item.
>   ```
>
>   ###### 代码块
>
>   ```markdown
>   1.  Open the file.
>   2.  Find the following code block on line 21:
>   
>           <html>
>             <head>
>               <title>Test</title>
>             </head>
>   
>   3.  Update the title to match the name of your website.
>   ```
>
>   ###### 图片
>
>   1. Open the file containing the Linux mascot.
>
>   2.  Marvel at its beauty.
>
>       ![Tux, the Linux mascot](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAB5CAMAAAD4dHQjAAAArlBMVEX////Hx8ePj49XV1c7OzsfHx9JSUlzc3Orq6vx8fHj4+MtLS2BgYG5ubnV1dVlZWWdnZ3///9xXymZeBfClhWnghZwWxpzb2RVRxzdqhP4vhE6Mx2Mbxi0jBUtKR7qtBJxYzhXU0hJRTpIPRzPoBRjURt+ZRl+aShya1WBfXJyZ0b8/Pz5+fn9/f329vbu7u7p6enm5ubv7+/6+vro6Oj39/fz8/Ps7Oz09PTr6+uCz11jAAAAAXRSTlMAQObYZgAABuxJREFUeAG1mgd/ozwShy3gD4g+a/DZgsDeubwlcXq2fP8vdvEAgRCDUthne8EPoxkNI35ZfAhhmBZqbMeVi/nxHB+vcbyZFdLBGYJZHaGPs9gzrpmLIfNbQoxjz5VyH0Pmz4uJSaI5HALTmHNIYmiYYbtI6AhmKV8N1kyr9adTb0FL8mUJ9MSzFLA+KfNLbCcw0eerkqT+WNNHiy/oGc9Gh/iixGg+VjqtIyJG2vNKBJ2IwYTUIP2ZyoslZv9DbXrBQIvxdYlLNQ7Q/MFNOC1zSjyiMJFEIe/u2ubQM9aMEl4pp7lzIopamTmXJAGIXACSqJFwLoy+xPz6Zqw/VrxI4j8hEZyN0Pi2TNNs9R83aCXWjJKAq3e9UTVZ3uTEw1ySCLBOSSnUxbpQqlhfKFWUSDg3s7VhXpokU2pZVEpVxVIpVWXfDa83KRkzSPDfVA3ZFP/DrJJ1pc5QbWeTSGClzrDbqaqcKycCpcqqt47yQqn1fCW8Vau9GpCVmepJ/K/3rk213b+Kpdrn6emX2YZIB9irarXddYqiPLDzmz3XU8sCZ35ZFlVTuuVqw66/omSmU4rHef2+UZtVuS6KwzY/sEJlf3vkzTRE8s0aMjjlOc2Ki10dzuFv8eqpBecrEruZHdy/VEfxj0dMMMvAEgHNtBJ9awss/VdQQzTLUcjBCTrh/VOklUqX313qsGaYuj30pyAvMYwkopbBevnRl46kAY0Sff1IH4IJaRwb+FqFtXMoTZCgh/v5g1xME8gvnuvapLo0hYMevvzkqdeXNIX4yvucyEeNQ9NYwGeT3x1xBE2TfP4k7KDBIg3SxyeTH6IlIQ2c+sD9eFo8H13adXjcUzpL8JF2Yvj6tDMOW4IPdn3OpcV9ySM9og45RoMl391OhIP6TKqHAze7CjDe205iT1u/gw3phh+YkOpLvLgLRIszWDDzfY91R4y0rcur4/kCAwzv3bl360DMtxvxeH1ze3fi9v76eDYU572hWBxI9CaQh/u7Ho/3l29DSXShDALh2u9H0So65HFYYHbX+GN9ICb59QG35enx7i23D/RCWN/b+wrM7VWjpJYfd+e5HnT8hPR7pS0tn8N2qIWXSmMxwJd02143MTp8X2HfobV49TInaIk0jxFX9tMu7xidxeZrBPTr5ddJE73Vur6b5qG/XiT1w3EIhkS3SS4fNZLbIzFRHT1emJ6CTJZ4xPy803FDNT73OmiTYnUSi5hfd3quiIkBpy8Jx14MdJK43uiPd3puX5LikoQu86KVSMDQV9agwqQwJAmtxGglZCPkrOsFXShMZOkkAWqIEghdIINQ+Ih0usi1piUmagRJX+oyMgjFxQnf8Tg5WgkXidHfh3oeukntWRP5Wkk30f1+r+PxIUGHwRaNhEPp0q7n5igd9HB48YRGArfbiHruKfTxCleOP7Zi5EVR7Hnq1HWUjt/kgi/qMGn0gRJgValndts6lnfW1lWETBXo43hjx6EQ+/4rzFgeb97j+EnmXqkdhljh2eNCXqm+xQ95ftDxFGGnlMrRsUzTtMjPnSFs/r9Me2OxvNRX8dHI1TMZWsqseYeIYSwyxlL1WNUhR3SvX61DewWzbu+1yl/n3gt8HFSfDWpcnUUSMsUc8Mw+VS8U/eeWawPbVL2mQE2isfzw0F5apengRo1WEVnIDzs1oHt9bUxbrgQqNUJutg6fE/WWZbf7f09JXKgxVmhbSbkbuw80RFMTy6VRqjGWEHUgKNQIGRqskVmYIWOtxtg0W8VAqsZYo8E9fk6itr6sJWqUFA023YxLzAlJiriW7NRE4hrG1+uRO9coB+6TLlI1SpVrJLzhCzUB9xaBCzXODkxMvzUbfuI+fbmQdXVNVpgjr8Z7sMBGTZGetj32aooVQj4xanrwBFtrsbDWmv9j09NkD16paQroJVUZTBTwk8SFXuLhoKZJEY5Nkj8fKIDSkFuLBDulofDl5c05xRVF9nRK+fmYLMxSacktQVdDzf0V8WuLpZoiyxFoi4tJgTiiy1837ar9vn86UuTgxG5KsYZ/es6jUHrWAEzXI6KrZy6JKDIsMOVkFL4hF++VZGDs2DCESIzA9NEyWpsXtYLBSumpMMrIPW7WjYIxy0rp+ahkCQSyfxbNL2aXrGBHwy9tLfdFlu70zVgvaR2OHM6OblznsVyvlunE4GLFRij4ak+4RmxyfR3OOhZD2qsCvqpcDT08bxpJ/fFD7FzvGCKSGNhevLlMjL/oW77Nx0KLNHzky6pXjXsEi1HscjdoVTGHrNdYwLpINyfDxWr6dWLk9y1Z2d2QltDx0RJzNU5ZijruKsthicVHiFzjRKgN3jOB9f6w3gJWt8lnRzg2ANMJF+f5P0W+bcQV1sTgAAAAAElFTkSuQmCC)
>
>   3. Close the file.
>
> * ###### 代码段
>
>   ```markdown
>   1. First item
>   2. Second item
>   3. Third item
>       - Indented item
>       - Indented item
>   4. Fourth item
>   ```

---

## Markdown代码语法

> ##### At the command prompt, type `nano`.
>
> > 将代码放在 ```反引号中``
>
> ##### 转义反引号
>
> > 如果你要表示为代码的单词或短语中包含一个或多个反引号，则可以通过将单词或短语包裹在双反引号
> >
> > `Use `code` in your Markdown file.
> >
> > ```markdown
> > ``Use `code` in your Markdown file.`
> > ```
>
> ##### 围栏代码块
>
> ````markdown
> ```bash
> 代码
> ```
> ````

---

## Markdown删除语法

```text
~~世界是平坦的。~~ 我们现在知道世界是圆的。
```

~~世界是平坦的。~~ 我们现在知道世界是圆的。

---

## Markdown表情语法

https://emojipedia.org/

> ![Tux, the Linux mascot](https://gw.alipayobjects.com/mdn/rms_ee84bf/afts/img/A*uz-1TKW-ni8AAAAAAAAAAAAAARQnAQ)
>
> > ###### 图片
> >
> > > ![这是图片](https://markdown.com.cn/assets/img/philly-magic-garden.9c0b4415.jpg)
> >
> > ```markdown
> > ![图片alt](图片链接 "图片title")
> > ```
> >
> > ###### 给图片附带链接
> >
> > > [![沙漠中的岩石图片](https://markdown.com.cn/assets/img/shiprock.c3b9a023.jpg "Shiprock")](https://markdown.com.cn/)
> > >
> > > ```markdown
> > > [![沙漠中的岩石图片](/assets/img/shiprock.jpg "Shiprock")](https://markdown.com.cn)
> > > ```

上面是图片语法

---

## Markdown表格语法

```text
| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |
```

---

## Markdown链接语法

[超链接显示名](超链接地址 "超链接title")

```markdown
[超链接显示名](超链接地址 "超链接title")
```