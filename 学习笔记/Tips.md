# Tips

## 目录

-   [kali怎么设置中文](#kali怎么设置中文)
-   [Kali 终端光标设置为下划线](#Kali-终端光标设置为下划线)
-   [Kali 设置外观窗口](#Kali-设置外观窗口)
-   [1Panel 配置](#1Panel-配置)
    -   [一、安装](#一安装)
    -   [二、启动](#二启动)

# **kali怎么设置中文**

1.  进入root模式，打开终端并输入以下命令：sudo -i
2.  更新软件源，输入以下命令：apt-get update
3.  安装中文字体，输入以下命令：apt install ttf-wqy-zenhei
4.  设置系统语言区域，输入以下命令：sudo dpkg-reconfigure locales
5.  在选择语言区域的界面，使用滚轮或向下箭头选择中文选项（zh\_CN.UTF-8 UTF-8），然后按回车键确认选择。
6.  再次选择中文选项（zh\_CN.UTF-8），按回车键确认选择。
7.  安装完成后，重启系统。 可以通过命令行输入reboot或手动选择重启。

# Kali 终端光标设置为下划线

# Kali 设置外观窗口

-   **Kali->设置->外观->设置-> Windows缩放**
-   Kali->设置->外观->窗口管理器->主题：Kali-Dark-xHiDPI

***

# 1Panel 配置

> [官网](https://1panel.cn/ "官网") | [文档](https://1panel.cn/docs/installation/online_installation/ "文档")

## 一、安装

```bash
# Centos | RedHat
curl -sSL https://resource.fit2cloud.com/1panel/package/quick_start.sh -o quick_start.sh && sh quick_start.sh

```

```bash
# Ubuntu
curl -sSL https://resource.fit2cloud.com/1panel/package/quick_start.sh -o quick_start.sh && sudo bash quick_start.sh

```

## 二、启动

> **1pctl user-info** 查看端口及地址：http\://目标服务器 IP 地址:目标端口/安全入口

```bash
1panel user-info
```

***

![](image/redis服务_u95oKgz8A-.png)

![](image/a_aHfR9XjgSu.png)

---
## Centos7 设置系统语言支持

```shell
# 查看是否支持
locale -a

# 寻找是否支持语言组
yum groupinstall "fonts"

# 设置语言
localectl set-locale LANG=zh_CN.UTF-8

bash -c "$(curl -L gitee.com/mo2/linux/raw/2/2)"
```