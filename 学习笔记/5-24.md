# Go并发编程

> 并发：多对一
>
> 并行：多对多

---

## 协程 goroutine 的引入

> 单线程下的并发，微线程。
>
> 多任务的另一种方式，比线程更小的执行单元。自带`cpu`的上下文，方便切换。

## 并发安全-资源竞争

> 并发会导致，多个协程同时读写同一个参数。数据的不确定性，并发不安全。

```shell
# 查看是否有资源竞争
go build -race main.go
```

## 解决方案

> 加锁 => 互斥锁
>
> mutex  sync.Mutex  **// 互斥锁**
>
> rwmutex sysc.RWMutex **// 读取锁**

---

### 1、加锁

```go
// 加锁
var (
	mutex   sync.Mutex   // 互斥锁
	remutex sync.RWMutex // 读取锁
	sum     int          // 和
)

// 协程 添加 读锁 => 所有协程都是读取一致的
func readSum() string {
	remutex.Lock()
	defer remutex.Unlock()
	b := sum
	return strconv.Itoa(b)
}

// 加法函数
func add(i int) {
	mutex.Lock()
	defer mutex.Unlock()
	sum += i
}

func main() {
	// 并发计数器
	var wg sync.WaitGroup

	wg.Add(110)

	// 加法操作
	for i := 0; i < 100; i++ {
		go func() {
			wg.Done()
			add(10)
		}()
	}

	// 读取操作 => 相同数据
	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			fmt.Println("和为: " + readSum())
		}()
	}

	wg.Wait()
}
```

---

`加锁版阶乘`

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

var (
	lock    sync.Mutex
	testMap = make(map[int]int, 10)
)

func testMum(num int) {
	lock.Lock()
	res := 1
	for i := 1; i < num; i++ {
		res += i
	}
	testMap[num] = res
	lock.Unlock()
}

func main() {
	start := time.Now()
	for i := 0; i < 20; i++ {
		go testMum(i)
	}

	lock.Lock()
	// 协程需要在main之后完毕
	for key, value := range testMap {
		fmt.Printf("数字%v 对应的阶乘是 %v\n", key, value)
	}
	lock.Unlock()

	end := time.Since(start)
	fmt.Println(end)
}

```

---

### 2、channel 通道

![image-20230525203730917](C:\Users\27450\AppData\Roaming\Typora\typora-user-images\image-20230525203730917.png)

> 通道是一种队列的数据结构，FIFO先进先出
>
> 1. `创建` =>
>
>    ```go
>    /* 
>    make(chan int, len, cap)
>    	以定义各种类型 []*demoType | int | string | bool
>    */
>    ```
>
> 2. 获取数据
>
>    ```go
>    demoChan := make(chan int, 10)
>    getInfo := <- demoChan // 获取到数据
>    ```
>
> 3. 传入数据
>
>    ```go
>    demoChan := make(chan int, 10)
>    demoChan <- 1// 传入数据
>    ```
>
> 4. 案例
>
>    ```go
>    var intChan chan int
>    
>    func main() {
>    	intChan = make(chan int, 10)
>    	// 传入值
>    	intChan <- 1
>    	// 输出 通道的地址（指针）
>    	fmt.Println(intChan)
>    	// 输出大小 | 容量
>    	fmt.Println(len(intChan), cap(intChan))
>    	// 输出 数值 | 地址
>    	fmt.Printf("[Value]=> %v, [Address]=> %v", <-intChan, intChan)
>    }
>    ```

---

```go
package main

import "fmt"

type Animals struct {
	item  string
	color string
}

func main() {
	// 初始化一个通道
	animal := make(chan interface{}, 10)
	// 向通道传入数据
	animal <- Animals{
		item:  "小黄",
		color: "黄色",
	}
	// 类型断言 | 获取到类型数据
	dog := (<-animal).(Animals)
	fmt.Printf("dog.item: %v\n", dog.item)
	fmt.Printf("dog.color: %v\n", dog.color)
}

```

#### `关闭通道`

```go
// 关闭
close(intChan)

// 遍历
for val := range inChan{
    fmt.Println(val)
}
```

```go
// 只用 for + len 取出来是不对的, len发生了变化
for i := 0; i <= len(intChan); i++ {
	fmt.Println(<-intChan)
}
```

```go
// 使用 for{}
for {
    val, ok := <-animal
    if !ok {
        break
    }
    fmt.Println(val)
}
```

