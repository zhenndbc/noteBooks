# Docker 容器技术

> 前言：--------------->>> 献给那些不计成败、勇于尝试的人们。
>
> ​		容器是轻量且可移植的仓库，包含应用程序及其依赖的组件。

```bash
systemctl start docker # 启动docker服务(停止同理)
```

配置文件（ssh连接使用）

```bsh
# 配置网络
vi /etc/sysconfig/network-scripts/ifcfg-ens33
```

重启网络服务

```bash
systemctl restart network.service # 重启网络服务
service network restart # 重启服务
```

配置文件

```bash
# config-network
TYPE=Ethernet                        //网络类型：Ethernet以太网
BOOTPROTO=none                       //引导协议：自动获取、static静态、none不指定
DEFROUTE=yes                         //启动默认路由
IPV4_FAILURE_FATAL=no                //不启用IPV4错误检测功能
IPV6INIT=yes                         //启用IPV6协议
IPV6_AUTOCONF=yes                    //自动配置IPV6地址
IPV6_DEFROUTE=yes                    //启用IPV6默认路由
IPV6_FAILURE_FATAL=no                //不启用IPV6错误检测功能
NAME=eno16777736                     // 网卡设备的别名
UUID=90528772-9967-46da-b401-f82b64b4acbc         //网卡设备的UUID唯一标识号
DEVICE=eno16777736                   // 网卡的设备名称
ONBOOT=yes                           //开机自动激活网卡
DNS1=6.6.6.6                         //DNS域名解析服务器的IP地址
IPADDR=192.168.1.199                 //网卡的IP地址
PREFIX=24                            //子网掩码
GATEWAY=192.168.1.1                  //默认网关IP地址
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPADDR=192.168.2.2             #你想要设置的固定IP，理论上192.168.2.2-255之间都可以，请自行验证；
NETMASK=255.255.255.0          #子网掩码，不需要修改；
GATEWAY=192.168.2.1            #网关，这里是你在“2.配置虚拟机的NAT模式具体地址参数”中的
```

完整文件

```bash
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="static"
IPADDR="192.168.110.133"
NETMASK="255.255.255.0"
DNS1="8.8.8.8"
DNS2="1.2.4.8"
GATEWAY="192.168.110.2"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="yes"
IPV6INIT="no"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33"
UUID="ff50d26b-a431-41e3-a257-2143266fda98"
DEVICE="ens33"
ONBOOT="yes"
```

yum.repo文件下修改yum源(阿里源)

```bash
# CentOS-Base.repo
#
# The mirror system uses the connecting IP address of the client and the
# update status of each mirror to pick mirrors that are updated to and
# geographically close to the client.  You should use this for CentOS updates
# unless you are manually picking other mirrors.
#
# If the mirrorlist= does not work for you, as a fall back you can try the
# remarked out baseurl= line instead.
#
#

[base]
name=CentOS-$releasever - Base - mirrors.aliyun.com
failovermethod=priority
baseurl=http://mirrors.aliyun.com/centos/$releasever/os/$basearch/
        http://mirrors.aliyuncs.com/centos/$releasever/os/$basearch/
        http://mirrors.cloud.aliyuncs.com/centos/$releasever/os/$basearch/
gpgcheck=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

#released updates
[updates]
name=CentOS-$releasever - Updates - mirrors.aliyun.com
failovermethod=priority
baseurl=http://mirrors.aliyun.com/centos/$releasever/updates/$basearch/
        http://mirrors.aliyuncs.com/centos/$releasever/updates/$basearch/
        http://mirrors.cloud.aliyuncs.com/centos/$releasever/updates/$basearch/
gpgcheck=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras - mirrors.aliyun.com
failovermethod=priority
baseurl=http://mirrors.aliyun.com/centos/$releasever/extras/$basearch/
        http://mirrors.aliyuncs.com/centos/$releasever/extras/$basearch/
        http://mirrors.cloud.aliyuncs.com/centos/$releasever/extras/$basearch/
gpgcheck=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

#additional packages that extend functionality of existing packages
[centosplus]
name=CentOS-$releasever - Plus - mirrors.aliyun.com
failovermethod=priority
baseurl=http://mirrors.aliyun.com/centos/$releasever/centosplus/$basearch/
        http://mirrors.aliyuncs.com/centos/$releasever/centosplus/$basearch/
        http://mirrors.cloud.aliyuncs.com/centos/$releasever/centosplus/$basearch/
gpgcheck=1
enabled=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

#contrib - packages by Centos Users
[contrib]
name=CentOS-$releasever - Contrib - mirrors.aliyun.com
failovermethod=priority
baseurl=http://mirrors.aliyun.com/centos/$releasever/contrib/$basearch/
        http://mirrors.aliyuncs.com/centos/$releasever/contrib/$basearch/
        http://mirrors.cloud.aliyuncs.com/centos/$releasever/contrib/$basearch/
gpgcheck=1
enabled=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7
```

centos镜像

```bash
# 率先备份文件
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup

# centos 8
wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo
// 或者
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo

# centos 7
wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
// 或者
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo

----------------------------------------
yum clean all     # 清除系统所有的yum缓存 |
yum makecache     # 生成yum缓存         |

# 查看yum源
yum repolist all

# 查看可用yum源
yum repolist enabled

```
配置 Go
```bash
# 下载 golang (从go中文网下载)
wget https://studygolang.com/dl/golang/go1.18.2.linux-amd64.tar.gz
```

配置 python

```bash
# 安装依赖
yum install openssl-devel bzip2-devel expat-devel gdbm-devel readline-devel sqlite-devel gcc gcc-c++ openssl-devel libffi-devel python-devel mariadb-devel
# 安装python3.8.6
wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz
```

在centos中配置安装goland编译器

```bash
# 1. 解压重命名--移动目录
~$ tar -zxvf goland压缩包名  -C /usr/local
~$ sudo mv /usr/local/GoLand-2019.1.3/ /usr/local/GoLand
~$ ll /usr/local/
total 48
drwxr-xr-x 12 root root 4096 7月  23 17:08 ./
drwxr-xr-x 10 root root 4096 2月  10 08:12 ../
drwxr-xr-x  2 root root 4096 2月  10 08:12 bin/
drwxr-xr-x  2 root root 4096 2月  10 08:12 etc/
drwxr-xr-x  2 root root 4096 2月  10 08:12 games/
drwxr-xr-x 10 root root 4096 7月   9 05:29 go/
drwxr-xr-x  8 root root 4096 7月  23 17:06 GoLand/
drwxr-xr-x  2 root root 4096 2月  10 08:12 include/
drwxr-xr-x  3 root root 4096 6月  30 00:32 lib/
lrwxrwxrwx  1 root root    9 6月  29 23:43 man -> share/man/
drwxr-xr-x  2 root root 4096 2月  10 08:12 sbin/
drwxr-xr-x  6 root root 4096 2月  10 08:15 share/
drwxr-xr-x  2 root root 4096 2月  10 08:12 src/

# 2. 将启动脚本加入到用户目录下(/usr/local/)即可在任意位置启动
~$ cd /usr/local/GoLand/bin/
~$ sudo ln -s $(pwd)/goland.sh /usr/bin/goland.sh

# 2. 配置别名独立启动、运行
~$ vim .bashrc
...
alias goland='nohup goland.sh & >/dev/null'
```

修改配置文件后，立即生效命令（source）

```bash
source命令通常用于重新执行刚修改的初始化文件，
	使之立即生效，而不必注销并重新登录。
-- 文件修改后，要么直接运行。要么使用命令改变。
source ./filename
```

> 关于goland使用破解版，导致无法启动的原因分析

```bash
# 首先获取权限(给run.sh权限)
chmod +x run.sh
# 运行
./run.sh

# 内部结构图如下：
|-- jihuo
|  -- _auto
|	 - important.txt
|    - micool.jar
|  -- JetBrains
|    - GoLand2022.1
|       - goland.key
|       - goland.vmoptions
# 实际上就是将文件中的内容进行替换了
```





