# 😀

### 手动抛出异常`raise`

```python
try:
    import uvicorn
    from fastapi.responses import Response
    from fastapi import FastAPI, Request, UploadFile, status
    from starlette.websockets import WebSocket, WebSocketState, WebSocketDisconnect
except ImportError:  # pragma: no cover
    raise ImportError(
        "Please install FastAPI by using `pip install nonebot2[fastapi]`"
    ) from None
```

---

### yaml文件处理

```python
def yaml_read_write_file(
        file_name: str = "config.yaml",
        mode: str = "r", my_dict: Optional[dict] = None,
        file_path: Optional[Path] = None
):
    """
    yaml文件读取写入
        - `file_name`: 文件名 [config.json]
        - `mode`: 文件模式 'r' or 'w'
        - `my_dict`: 写入的数据
        - `file_path`: 文件路径
    """
    try:
        if file_path is None:
            if not (file_path := Path(__file__).parents[1] / "res").exists():
                file_path.mkdir()

        if mode == "r":
            with open(file_path / file_name, 'r') as file_ptr:
                yaml_content = yaml.load(
                    file_ptr.read(), yaml.FullLoader
                )

            return yaml_content

        if mode == "w" and my_dict is not None:
            with open(file_path / file_name, mode="w", encoding="utf-8") as file_ptr:
                yaml.dump(my_dict, file_ptr)
            loggings.info(f"File {file_name} is Saved!")
        else:
            raise ValueError("dict must be not None")

    except ValueError or Exception as err:
        loggings.error(err)

```

---

### 检测文件夹

```python
def assert_file_exists(file_load: Optional[Path] = None):
    """ 检测 [资源] 目录是否存在, 不在则创建

        >>> assert_file_exists()
        'E:/src/res'
    """
    if file_load is None:
        if not (file_load := Path(__file__).parents[1] / "res").exists():
            file_load.mkdir()
        return file_load
    
    if not file_load.exists():
        file_load.mkdir()
    return file_load
```

