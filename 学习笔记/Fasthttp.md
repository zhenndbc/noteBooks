# Fasthttp

`server`

```go
package main

import (
	"fmt"

	"github.com/valyala/fasthttp"
)

func fooHandler(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(
		ctx, "Hello, world! Requested path is %q.",
		ctx.Path(),
	)
}

func barHandler(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(
		ctx, "Hello, world! Requested path is %q.",
		ctx.Path(),
	)
}

func main() {
	// 请求响应
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		switch string(ctx.Path()) {
		case "/foo":
			fooHandler(ctx)
		case "/bar":
			barHandler(ctx)
		default:
			ctx.Error("Unsupported path", fasthttp.StatusNotFound)
		}
	}

	// 监听服务
	fasthttp.ListenAndServe(":8080", requestHandler)
}

```

---

`api-key`

```bash
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
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
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
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
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
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
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
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
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
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

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
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
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
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
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
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
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
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
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
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
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
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
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
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
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
```

```shell
200287Lq!
```

`Go环境变量配置`
```shell
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
```
`.bashrc`
```shell
echo "ecport PATH=/usr/local/go/bin:$PATH" >> ~/.bashrc
```

> go mod tidy报错verifying module: invalid GOSUMDB: malformed verifier id
```shell
# 查看依赖图
go mod graph
# 设置sum
go env -w GOSUMDB="sum.golang.org"
```