# Docker配置nginx

> 前言
>
> docker的兴起是一股热潮，涌现了许多人才、技术。
>
> ![嘿嘿嘿](https://pic3.zhimg.com/v2-faa612ecf047ff4ac4eebf5864ad12aa_b.jpg)

1. Centos 7下 docker中配置nginx环境

   > 拉取镜像地址 -->> [配置教程](https://www.jianshu.com/p/b6b40ac1a329)
   >
   > ```bash
   > # 配置docker
   > docker pull nginx
   > 
   > # 创建docker中nginx环境
   > docker run -tid --name my-nginx -p 8080:80 -d nginx
   > 
   > # 在本地增加环境
   > mkdir /etc/docker/nginx-config/html  #存放静态文件 , (自己写个简单的 index.html 放里面吧)
   > 
   > mkdir /etc/docker/nginx-config/log #日志文件 
   > 
   > mkdir /etc/docker/nginx-config/config  #存放nginx配置文件 
   > 
   > 
   > ```
   >
   > ![hhh](https://pic2.zhimg.com/v2-d34e6d46392ad969b13a01cb26962cb9_b.jpg)