使用dockerfile编写一个python3.8.5的环境，需要具备`lxml`、`bs4`、`requests`、`pillow`、`fastapi[all]`的环境，并对外提供一个端口8080和一个用于ttyd的web bash端口8022，与外部主机有文件映射为`/root/sk-spider-image:/code/spider`

```dockerfile
FROM python:3.8.5

WORKDIR /code

# 安装所需的Python库
RUN pip install lxml bs4 requests pillow fastapi[all]

# 安装ttyd
RUN apt-get update && \
    apt-get install -y build-essential git vim-tiny && \
    git clone https://github.com/tsl0922/ttyd.git /tmp/ttyd && \
    cd /tmp/ttyd && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install && \
    cd / && \
    rm -rf /tmp/ttyd && \
    apt-get remove -y build-essential git && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# 设置工作目录和环境变量
ENV PYTHONPATH=/code
ENV PORT=8080
ENV TTYD_PORT=8022

# 对外暴露端口
EXPOSE $PORT
EXPOSE $TTYD_PORT

# 将文件映射到容器中
VOLUME /root/sk-spider-py:/code/spider

# 启动命令
CMD ["ttyd", "-p", "$TTYD_PORT", "bash"]

```



```python
# -*- coding:UTF-8 -*-
try:
    from icecream import ic
    import ddddocr
except ImportError as error:
    from re import search
    from os import system
    from sys import executable
    system('{} -m pip install {}  -i https://pypi.tuna.tsinghua.edu.cn/simple'
           .format(executable, search("'(.*?)'", str(error))[1]))

```













```dockerfile
FROM python:3.8.5-alpine

WORKDIR /code

# 安装依赖
RUN apk add --no-cache build-base git cmake \
    && pip install lxml bs4 requests pillow fastapi[all] \
    && git clone https://github.com/tsl0922/ttyd.git /tmp/ttyd \
    && cd /tmp/ttyd/build \
    && cmake .. \
    && make \
    && make install \
    && rm -rf /tmp/ttyd \
    && apk del build-base git \
    && rm -rf /var/cache/apk/*

# 设置工作目录和环境变量
ENV PYTHONPATH=/code
ENV PORT=8080
ENV TTYD_PORT=8022

# 对外暴露端口
EXPOSE $PORT
EXPOSE $TTYD_PORT

# 将文件映射到容器中
VOLUME /root/sk-spider-image:/code/spider

# 启动命令
CMD ["ttyd", "-p", "$TTYD_PORT", "bash"]

```

**构建容器**

```shell
# 构建 Dockerfile
docker build -t 容器名 .
```

一旦镜像构建成功，使用以下命令来运行容器：

```
docker run -p 8080:8080 -p 8022:8022 -v /root/sk-spider-image:/code/spider my-image-name
```

在这里，我们将本地端口8080映射到容器内部的端口8080，将本地端口8022映射到容器内部的端口8022，并将主机上的 `/root/sk-spider-image` 目录映射到容器内部的 `/code/spider` 目录。

当容器启动时，你可以访问 `http://localhost:8080/docs` 来查看 FastAPI 的 API 文档。如果要使用 ttyd 访问容器终端，则可以使用 `http://localhost:8022`。



# 同时运行版

```dockerfile
# 使用 Python 3.8.5 作为基础镜像，并替换为清华源
FROM python:3.8.5

# 设置清华源
RUN sed -i 's/deb.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list
RUN sed -i 's/security.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list

# 安装所需软件
RUN apt-get update && apt-get install -y \
    wget \
    unzip

# 下载并解压 ttyd 二进制文件
RUN wget https://github.com/tsl0922/ttyd/releases/download/1.7.3/ttyd.x86_64 -O /usr/local/bin/ttyd && \
    chmod +x /usr/local/bin/ttyd

# 设置容器内的工作目录
WORKDIR /code

# 复制项目文件到容器内
COPY . /code

# 安装 Python 依赖包
COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# 对外暴露 FastAPI 端口
EXPOSE 8000

# 对外暴露 ttyd Web Bash 端口
EXPOSE 8022

# 启动 ttyd 和 FastAPI 应用
CMD ["ttyd", "-p", "8022", "bash", "&", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

```







```dockerfile
ROM python:3.8.5

# 设置清华源
RUN sed -i 's/deb.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list && \
    sed -i 's/security.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list

# 安装所需软件
RUN apt-get update && apt-get install -y \
    wget \
    unzip

# 下载并解压 ttyd 二进制文件
RUN wget https://github.com/tsl0922/ttyd/releases/download/1.7.3/ttyd.x86_64 -O /usr/local/bin/ttyd && \
    chmod +x /usr/local/bin/ttyd

# 设置工作目录
WORKDIR /code/spider

# 将外部主机的文件映射到容器内的目录
VOLUME /root/sk-spider-img:/code/spider

# 安装 Python 依赖包
COPY requirements.txt /tmp/requirements.txt

# 安装 Python 依赖包
RUN pip install --no-cache-dir -r /tmp/requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

# 暴露 FASTAPI 的默认端口
EXPOSE 8000
EXPOSE 8022


# 启动 ttyd 和 FastAPI 应用
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

ttyd -p 8022 -c admin:amdin bash

# 删除一些配置
docker build -t sk-api-img:V1.2 . && docker rm 30 && docker rmi sk-api-img:V1.1

# 启动 ttyd 和 FastAPI 应用
CMD ["ttyd", "-p", "8022", "bash"]

# 启动 ttyd 和 FastAPI 应用
CMD ["ttyd", "-p", "8022", "-c", "admin:amdin" "sh", "&", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
```







# 稳定版

```dockerfile
FROM python:3.8.5

# 设置清华源
RUN sed -i 's/deb.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list && \
    sed -i 's/security.debian.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apt/sources.list

# 设置工作目录
WORKDIR /code/spider

# 将外部主机的文件映射到容器内的目录
VOLUME /root/sk-spider-img:/code/spider

# 安装依赖包
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple fastapi[all] ddddocr pyjwt


# 暴露 FASTAPI 的默认端口
EXPOSE 20000

# 设置容器启动时的默认命令
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "20000"]
```

# 稳定版启动

```shell
# 构建
docker build -t sk-api-img .

# 创建spider
mkdir -p /code/spider

docker run -d -p 8000:20000 -v /code/spider/spider-img:/code/spider --name spider-api-img sk-api-img


# 启动
docker run -d -p 8000:8000  -p 8022:8022 -v /root/sk-spider-img:/code/spider --name spider-api-img sk-api-img


# 启动
docker run -d -p 9000:8000 -p 9022:8022 -v /root/sk-spider-img:/code/spider --name spider-api-img sk-api-img
```

```shell
version: "3.3"
services:
  docker:
    container_name: code-server
    environment:
      - DEFAULT_WORKSPACE=/config/workspace
      - PASSWORD=root
      - SUDO_PASSWORD=root
    network_mode: host
    volumes:
      - /home/skong/project/code-server/config:/config
      - /home/skong/manage:/config/workspace
      - /run/docker.sock:/var/run/docker.sock
      - /usr/bin/docker:/usr/bin/docker
      - /usr/bin/docker-compose:/usr/bin/docker-compose
    image: testkong/code-server-forubt:v1.1
    restart: always
    privileged: true
    depends_on: []
    networks: []
    ports: []
```

```json
{
  // 修改注释颜色
  "editor.tokenColorCustomizations": {
    "comments": {
      "foreground": "#43827a"
    }
  },
  // 复制当前行
  "editor.emptySelectionClipboard": true,
  // 鼠标缩放字体
  "editor.mouseWheelZoom": true,
  // 配置文件类型识别
  "files.associations": {
    "*.js": "javascript",
    "*.json": "jsonc",
    "*.cjson": "jsonc",
    "*.wxss": "css",
    "*.wxs": "javascript",
    "*.lua": "lua"
  },
  // // 文件隐藏
  // "files.exclude": {
  //   "**/.DS_Store": true,
  //   "**/.git": true,
  //   "**/.hg": true,
  //   "**/.svn": true,
  //   "**/CVS": true,
  //   "**/node_modules": false,
  //   "**/tmp": true
  // },
  // "background.customImages": [
  //   "file:///C:/Users/skong/Documents/BackGround.jpg"
  // ],

  // Todo Tree plugin config
  "todo-tree.highlights.defaultHighlight": {
    "type": "text",
    "foreground": "#ffff00",
    "background": "#aaa",
    "opacity": 0.5,
    "iconColour": "#ffff00",
    "gutterIcon": true
  },
  // 只显示指定目录下文件
  "todo-tree.filtering.includeGlobs": ["**/manage/*/**"],
  "todo-tree.highlights.customHighlight": {
    "BUG": {
      "icon": "bug",
      "foreground": "#F56C6C",
      "type": "line"
    },
    "FIXME": {
      "icon": "flame",
      "foreground": "#FF9800",
      "type": "line"
    },
    "TODO": {
      "foreground": "#FFEB38",
      "type": "line"
    },
    "NOTE": {
      "icon": "note",
      "foreground": "#67C23A",
      "type": "line"
    },
    "INFO": {
      "icon": "info",
      "foreground": "#909399",
      "type": "line"
    },
    "TAG": {
      "icon": "tag",
      "foreground": "#409EFF",
      "type": "line"
    },
    "HACK": {
      "icon": "versions",
      "foreground": "#E040FB",
      "type": "line"
    },
    "XXX": {
      "icon": "unverified",
      "foreground": "#E91E63",
      "type": "line"
    }
  },
  "todo-tree.general.tags": [
    "BUG",
    "HACK",
    "FIXME",
    "TODO",
    "INFO",
    "NOTE",
    "TAG",
    "XXX"
  ],
  "todo-tree.general.statusBar": "total",
  "background.style": {
    //样式配置
    "content": "''",
    "pointer-events": "none",
    "position": "absolute",
    "z-index": "99999",
    "width": "100%",
    "height": "100%",
    "background-position": "100% 100%",
    "background-repeat": "no-repeat",
    "opacity": 0.1,
    "background-size": "100%"
  },
  "editor.tabSize": 4,
  "editor.fontWeight": "300",
  "editor.tabCompletion": "on",
  "editor.fontFamily": "Fira code, Consolas, JetBrains Moon", // Firacode , JetBrains Moon
  "editor.minimap.enabled": true,
  "javascript.format.semicolons": "remove",
  // 指定 *.vue 文件的格式化工具为vetur
  "[vue]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  // 函数前面加个空格
  "javascript.format.insertSpaceBeforeFunctionParenthesis": true,
  "prettier.singleQuote": true,
  "prettier.semi": false,
  // react
  // 当按tab键的时候，会自动提示
  "emmet.triggerExpansionOnTab": true,
  "emmet.showAbbreviationSuggestions": true,
  "emmet.includeLanguages": {
    // jsx的提示
    "javascript": "javascriptreact",
    "vue-html": "html",
    "vue": "html",
    "wxml": "html"
  },
  // end
  "[jsonc]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "security.workspace.trust.untrustedFiles": "open",
  "git.ignoreMissingGitWarning": true,
  "editor.suggest.snippetsPreventQuickSuggestions": false,
  // 自动保存
  "editor.fontSize": 14,
  "editor.suggestSelection": "recentlyUsedByPrefix",
  // 保存时自动格式化 import
  "files.encoding": "utf8",
  "files.autoSave": "afterDelay",
  "editor.lineNumbers": "on",
  "editor.formatOnType": true,
  "editor.formatOnSave": true,
  "editor.formatOnPaste": true,
  "editor.cursorStyle": "line-thin",
  "editor.cursorSmoothCaretAnimation": "on",
  "terminal.integrated.cursorStyle": "line",
  "terminal.integrated.cursorBlinking": true,
  "terminal.integrated.copyOnSelection": true,
  "code-runner.clearPreviousOutput": true,
  "code-runner.saveFileBeforeRun": true,
  "code-runner.runInTerminal": true,
  "explorer.confirmDelete": false,
  "explorer.compactFolders": true,
  "explorer.confirmDragAndDrop": false,
  // 编辑器设置
  "editor.accessibilityPageSize": 700,
  "editor.quickSuggestionsDelay": 80,
  "editor.linkedEditing": true,
  "editor.suggest.preview": true,
  "editor.colorDecorators": true,
  "editor.suggest.showColors": true,
  "editor.guides.bracketPairs": true,
  "editor.suggest.showSnippets": true,
  "editor.snippets.codeActions.enabled": true,
  "editor.bracketPairColorization.enabled": true,
  "editor.cursorBlinking": "phase",
  "editor.snippetSuggestions": "bottom",
  "editor.scrollbar.horizontal": "visible",
  "editor.autoClosingBrackets": "beforeWhitespace",
  "editor.autoClosingQuotes": "beforeWhitespace",
  "editor.selectionHighlight": true,
  "editor.bracketPairColorization.independentColorPoolPerBracketType": true,
  // terminal 设置
  "terminal.integrated.inheritEnv": false,
  "terminal.integrated.enableMultiLinePasteWarning": false,
  "terminal.integrated.defaultProfile.windows": "Command Prompt",
  "terminal.integrated.fontFamily": " Consolas",
  "workbench.startupEditor": "none",
  "workbench.settings.editor": "json",
  // 插件更新设置
  "files.eol": "\n",
  "update.mode": "none",
  "extensions.autoUpdate": true,
  "extensions.ignoreRecommendations": false,
  // 远程连接
  "remote.SSH.remotePlatform": {
    "100.100.100.101": "linux",
    "Docker-CentOS7": "linux",
    "192.168.85.128": "linux",
    "100.100.100.103": "linux",
    "1.117.154.114": "linux",
    "100.100.100.102": "linux",
    "testkong.work": "linux",
    "123.207.17.178": "linux",
    "fromsko-screenshottocod-cavaxuxk8qy.vss.gitpod.io": "linux"
  },
  // 语言功能配置
  /* golang */
  "go.useLanguageServer": true,
  "go.formatTool": "goformat",
  "[go.mod]": {
    "editor.formatOnSave": true,
    "editor.codeActionsOnSave": {
      "source.organizeImports": true
    }
  },
  /* python */
  "[python]": {
    "editor.formatOnType": true,
    "editor.formatOnPaste": true,
    "editor.rulers": [120]
  },
  "workbench.colorCustomizations": {
    "editorRuler.foreground": "#ff1ea9"
  },
  "editor.rulers": [100],
  // Git设置
  "git.autofetch": true,
  "git.confirmSync": false,
  "git.enableSmartCommit": true,
  "git.openRepositoryInParentFolders": "never",
  "workbench.iconTheme": "material-icon-theme",
  "go.toolsManagement.autoUpdate": true,
  "javascript.updateImportsOnFileMove.enabled": "always",
  "git.ignoreLegacyWarning": true,
  "excalidraw.theme": "light",
  "markdown.preview.breaks": true,
  "editor.unicodeHighlight.includeStrings": false,
  "editor.unicodeHighlight.allowedLocales": {
    "ja": true
  },
  "workbench.editorAssociations": {
    "*.sbt": "default"
  },
  "workbench.colorTheme": "Solarized Dark",
  "remote.SSH.defaultExtensions": ["gitpod.gitpod-remote-ssh"],
  "CodeGPT.model": "gpt-3.5-turbo",
  "CodeGPT.query.language": "Chinese",
  "CodeGPT.temperature": 0.5,
  "CodeGPT.Autocomplete.maxTokens": 2000,
  "CodeGPT.maxTokens": 3000,
  "gopls": {
    "ui.semanticTokens": true
  },
  "[proto3]": { "editor.defaultFormatter": "zxh404.vscode-proto3" },
  "workbench.productIconTheme": "emoji-product-icons",
  "dev.containers.executeInWSL": true,
  "[html]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "tabnine.experimentalAutoImports": true,
  "window.zoomLevel": 1,
  "[javascript]": {
    "editor.defaultFormatter": "vscode.typescript-language-features"
  },
  "editor.codeActionsOnSave": {
    "source.fixAll": true,
    "source.organizeImports": true
  },
 // 原生复现 彩色括号
  "workbench.colorCustomizations": {
    "editorBracketHighlight.foreground1": "#ffd700",
    "editorBracketPairGuide.activeBackground1": "#ffd7007f",
    "editorBracketHighlight.foreground2": "#da70d6",
    "editorBracketPairGuide.activeBackground2": "#da70d67f",
    "editorBracketHighlight.foreground3": "#87cefa",
    "editorBracketPairGuide.activeBackground3": "#87cefa7f",
    "editorBracketHighlight.foreground4": "#ffd700",
    "editorBracketPairGuide.activeBackground4": "#ffd7007f",
    "editorBracketHighlight.foreground5": "#da70d6",
    "editorBracketPairGuide.activeBackground5": "#da70d67f",
    "editorBracketHighlight.foreground6": "#87cefa",
    "editorBracketPairGuide.activeBackground6": "#87cefa7f",
    "editorBracketHighlight.unexpectedBracket.foreground": "#ff0000",
  },
}
```