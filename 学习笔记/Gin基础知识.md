# Gin基础知识

## 基础案例

```go
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	原型 => type HandlerFunc func(*Context)
*/

func main() {
	r := gin.Default()          // 实例化一个对象
	r.GET("/ping", pong, hello) // 注册GET路由, 可以传递多个Context对象
	r.Run(":8090")              // TODO: 添加端口
}

func pong(c *gin.Context) {
	// TODO: 数据返回，传递一个 Context对象
	c.JSON(
		http.StatusOK, map[string]interface{}{
			"Message": "ping request",
		},
	)
}

func hello(c *gin.Context) {
	// TODO: 返回数据为 {"Message": "hello"}
	c.JSON(
		http.StatusOK, map[string]interface{}{
			"Message": "hello",
		},
	)
}

```

---

`函数原型`

```go
// 路由器 实例对象
func Default() *Engine {
	debugPrintWARNINGDefault()
	engine := New()
	engine.Use(Logger(), Recovery())
	return engine
}

// 事件响应
type HandlerFunc func(*Context)

// GET请求原型
func (group *RouterGroup) GET(relativePath string, handlers ...HandlerFunc) IRoutes {
	return group.handle(http.MethodGet, relativePath, handlers)
}

StatusOK = 200
```

---

`函数使用`

```go
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	两种创建方式 | 路由
		1. gin.Default()
			=> 提供了两种中间件 engine.Use(Logger(), Recovery())
			=> 事件恢复 Recovery | 日志记录 Logger

		2. gin.New()
			=> 对于panic等错误并不会返回任何信息,直接程序出错了
			=> 没有请求日志的捕获
*/

func main() {
	// TODO: 使用New()如下操作同上一致
	
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	r.GET("/demo", pong)
	r.Run(":9999")

	// TODO: Default增加了中间件,开发环境必备日志和恢复事件
	router := gin.Default()
	router.GET("/ping", pong)
	router.Run(":9090")
}

func pong(c *gin.Context) {
	c.JSON(http.StatusOK, map[string]interface{}{
		"Message": "Hello world",
	})
}

```

---

## 路由分组

`Group原型`

```go
func (group *RouterGroup) Group(relativePath string, handlers ...HandlerFunc) *RouterGroup {
	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		basePath: group.calculateAbsolutePath(relativePath),
		engine:   group.engine,
	}
}

// Group 是一个结构体,结构体实现了路由的所有方法
type RouterGroup struct {
	Handlers HandlersChain
	basePath string
	engine   *Engine
	root     bool
}

// GET is a shortcut for router.Handle("GET", path, handlers).
func (group *RouterGroup) GET(relativePath string, handlers ...HandlerFunc) IRoutes {
	return group.handle(http.MethodGet, relativePath, handlers)
}
```

`使用案例`

```go
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	路由分组
		api := router.Group("/api")

	后续只需要注册到其中就行
		{
			api.POST("/login", login),
			api.GET("/logout", logout),
			api.GET("/list", list),
		}
*/

func main() {
	router := gin.Default()

	// goods 分组
	goodsGroup := router.Group("/goods")
	{
		goodsGroup.GET("/list", goodsList)
		goodsGroup.POST("/login", goodsLogin)
		goodsGroup.GET("/logout", goodsLogout)
	}

	router.Run(":9090")
}

func goodsList(c *gin.Context) {
	c.JSON(http.StatusOK, map[string]interface{}{
		"data": "Goods List",
	})
}

func goodsLogin(c *gin.Context) {

}

func goodsLogout(c *gin.Context) {

}

```

---

## 获取传入参数

`函数原型`

```go
// ShouldBindUri binds the passed struct pointer using the specified binding engine.
func (c *Context) ShouldBindUri(obj any) error {
	m := make(map[string][]string)
	for _, v := range c.Params {
		m[v.Key] = []string{v.Value}
	}
	return binding.Uri.BindUri(m, obj)
}

// 模拟实现
func goodsLogout(c *gin.Context) {
	m := make(map[string][]string)

	// 内部通过遍历实现 获取 绑定的参数
	// TODO: c.ShouldBindUri(&person);
	for _, v := range c.Params {
		m[v.Key] = []string{v.Value}
		fmt.Println(v, m[v.Key])
	}
	c.JSON(http.StatusOK, gin.H{
		"data": "Goods List",
	})
}

```

`使用实例`

```go
package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	参数传递
		goodsGroup.GET("/list/:id/:action", goodsList)

			一般获取:
				-> 使用 c.Param("id") 接收参数

			规范获取:
				-> 使用 结构体 | c.ShouldBindUri(&person); 接收

		goodsGroup.GET("/list/:id/*action", goodsList)

			接收同上:
				-> 一般用于获取文件路径的
	UUID
		https://1024tools.com/uuid
*/

type Person struct {
	// ID   string `uri:"id" bingding:"required,uuid"`
	ID   int    `uri:"id" bingding:"required"`
	Name string `uri:"action" bingding:"required"`
}

func main() {
	router := gin.Default()

	goodsGroup := router.Group("/goods")
	{
		// 防止路径冲突
		goodsGroup.GET("/list/:id/:action", goodsList)
		goodsGroup.POST("/login/:username", goodsLogin)
		goodsGroup.GET("/logout/:username", goodsLogout)
	}

	router.Run(":9090")
}

func goodsList(c *gin.Context) {
	var person Person
	if err := c.ShouldBindUri(&person); err != nil {
		c.Status(404)
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id":   person.ID,
		"Name": person.Name,
	})
}

func goodsLogin(c *gin.Context) {

}

func goodsLogout(c *gin.Context) {
	// TODO: c.ShouldBindUri(&person);
	// 内部通过遍历实现 获取 绑定的参数

	m := make(map[string][]string)
	for _, v := range c.Params {
		m[v.Key] = []string{v.Value}
		fmt.Println(v, m[v.Key])
	}
	
	c.JSON(http.StatusOK, gin.H{
		"data": "Goods List",
	})
}

```

---

## 表单参数获取

`原型`

```go
func (c *Context) Query(key string) (value string) {
	value, _ = c.GetQuery(key)
	return
}

func (c *Context) DefaultQuery(key, defaultValue string) string {
	if value, ok := c.GetQuery(key); ok {
		return value
	}
	return defaultValue
}

// 原生获取
func (c *Context) GetQuery(key string) (string, bool) {
	if values, ok := c.GetQueryArray(key); ok {
		return values[0], ok
	}
	return "", false
}

func (c *Context) GetQueryArray(key string) (values []string, ok bool) {
	c.initQueryCache()
	values, ok = c.queryCache[key]
	return
}

```

`实例`

```go
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
	获取请求参数

		GET:
			默认参数 => DefaultQuery(arg1, arg2)
			仅有参数 => Query(arg)

		POST:
			默认参数 => DefaultPostForm(arg1, arg2)
			仅有参数 => PostForm(arg)

		可以混合使用 [ POST | GET ]:
*/

func main() {
	router := gin.Default()

	api := router.Group("/api")
	{
		api.GET("/login", loginHandler)
		api.POST("/form_post", form_post)
		api.POST("/post_req", post_reqHandler)
	}

	router.Run(":9090")
}

// TODO: 获取 GET 请求
func loginHandler(c *gin.Context) {
	// 含有默认参数 | 不带则使用 Query
	frist_name := c.Query("first_name")
	last_name := c.DefaultQuery("last_name", "skong")

	c.JSON(http.StatusOK, gin.H{
		"first_name": frist_name,
		"last_name":  last_name,
	})
}

// TODO: 获取 POST 请求 [表单请求]
func form_post(c *gin.Context) {
	// 含有默认参数 DefaultPostForm | 不带则使用 PostForm
	message := c.PostForm("message") // 未接受到为 ""
	nick := c.DefaultPostForm("nick", "Hello Nick")

	c.JSON(http.StatusOK, gin.H{
		"message": message,
		"nick":    nick,
	})
}

// TODO: 同时获取 POST 和 GET 参数
func post_reqHandler(c *gin.Context) {
	// GET 参数
	id := c.Query("id")
	page := c.DefaultQuery("page", "10")

	// POST 参数
	username := c.PostForm("username")
	password := c.DefaultPostForm("password", "admin")

	c.JSON(http.StatusOK, gin.H{
		"id":       id,
		"page":     page,
		"username": username,
		"password": password,
	})
}

```

---

