# Go语言环境搭建

```shell
# linux 中
/manager

# wget command

wget donwnload_url

wget -P /usr/local download_url

wget download_url -o /usr/local/go
```

---

## Go Proxy

> 代理能够加快服务器访问

---

`windows`

```shell
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
```

`linux`

```shell
export GO111MODULE=on
export GOPROXY=https://goproxy.cn
```

---

## install go

> 只介绍 `Linux` 环境下安装

---

```shell
# centos
yum install -y wget curl gcc
# download
wget -P ~/go https://studygolang.com/dl/golang/go1.20.4.linux-amd64.tar.gz && cd ~/go
# tar files
tar -C /usr/local -zxvf ./go1.20.4.linux-amd64.tar.gz
# set path
echo "export GOROOT=/usr/local/go" >> /etc/profile
echo "export PATH=$PATH:$GOROOT/bin" >> /etc/profile
source /etc/profile
```

---

### Cat go version

`version for golang`

```shell
go version
```

