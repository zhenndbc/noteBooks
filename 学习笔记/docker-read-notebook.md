# Docker 容器技术

> 前言：--->>> 献给那些不计成败、勇于尝试的人们。
>
>   容器是轻量且可移植的仓库，包含应用程序及其依赖的组件。

[https://mirrors.tuna.tsinghua.edu.cn/#](https://mirrors.tuna.tsinghua.edu.cn/%23)

 [github](https://qu.js.cn/how-to-speed-github/#:~:text=%E5%9B%BD%E5%86%85%E5%8A%A0%E9%80%9FGithub%E7%9A%84%E5%87%A0%E7%A7%8D%E6%96%B9%E6%A1%88%201%20GitHub%20%E9%95%9C%E5%83%8F%E8%AE%BF%E9%97%AE%201.github.com....%202%20raw%E6%96%87%E4%BB%B6%E4%B8%8B%E8%BD%BD%E5%8A%A0%E9%80%9F%203,%E9%80%9A%E8%BF%87%20Gitee%20%E5%AF%BC%E5%85%A5Github%E4%BB%93%E5%BA%93%E8%BF%9B%E8%A1%8C%E4%B8%AD%E8%BD%AC%E4%B8%8B%E8%BD%BD%206%20GitHub%20%E6%96%87%E4%BB%B6%E5%8A%A0%E9%80%9F%207%20%E7%BB%93%E6%9D%9F%E8%AF%AD)

进入网站，如下操作

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg)

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image004.jpg)

 

 

 

 

 

 

正在搭建三台服务器

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image006.jpg)

[ip配置，虚拟机](https://arong.blog.csdn.net/article/details/81507238?spm=1001.2101.3001.6650.4&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-4-81507238-blog-111901727.pc_relevant_scanpaymentv1&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-4-81507238-blog-111901727.pc_relevant_scanpaymentv1&utm_relevant_index=9)  service network restart 重启网络  参考文献 [配置](https://blog.csdn.net/qq_42180695/article/details/106359692?spm=1001.2101.3001.6650.3&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-3-106359692-blog-81507238.pc_relevant_antiscanv2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-3-106359692-blog-81507238.pc_relevant_antiscanv2&utm_relevant_index=6)

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image008.jpg)

 

 

 

 

 

 

如何配置git？

Linux中（centos7）

```bash
yum install git

git config --global user.email "1614355756@qq.com"

git config --global user.name “kong"

git config --global –list
```



使用 git clone 地址

 

 

\# 从阿里云下载docker容器

```bash
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```



去阿里云里面配置镜像加速地址

```bash
sudo mkdir -p /etc/docker

sudo tee /etc/docker/daemon.json <<-'EOF'

{

 "registry-mirrors": ["https://9tnchjtx.mirror.aliyuncs.com"]

}

EOF

sudo systemctl daemon-reload

sudo systemctl restart docker
```



 

\# 查看服务是否启动

```dockerfile
ps -ef | grep docker
```



\# 列出当前有的

```dockerfile
docker ps -a
```



\# 删除某条执行完毕后的命令

```dockerfile
docker rm 7e8245a34e71
```



\# 运行某个镜像

```dockerfile
sudo docker run hello-world
```



\# 重启docker服务

```dockerfile
sudo systemctl restart docker
```



\# 更改路径

```dockerfile
sudo systemctl daemon-reload
```



\# 下载docker compose

```bas
curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.0/docker-compose-`uname`-`uname -m` > /usr/local/bin/docker-compose
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image010.jpg)

\# 给定文件权限（可执行）

```bash
sudo chmod +x /usr/local/bin/docker-compose
```



\# 测试

```dockerfile
docker-compose -v
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image012.jpg)

\# 下载mysql镜像

```dockerfile
docker pull mysql:5.7
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image014.jpg)

\# 查看镜像文件

```dockrfile
docker images
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image016.jpg)

\# 设置MYSQL

```dockerfile
docker run -p 3306:3306 --name mymysql -v $PWD/conf:/etc/mysql/conf.d -v $PWD/log:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
```

\# 如果显示退出（使用命令查看日志）

```dockerfile
docker logs 225cf1784fc9
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image018.jpg)

 

\# 设置启动

找到mysql的id 225cf1784fc9

```dockerfile
docker ps -a 查到id
```



\# 配置

```dockerfile
docker exec -it 225cf1784fc9 /bin/bash
```

![img](file:///C:/Users/kong/AppData/Local/Temp/msohtmlclip1/01/clip_image020.jpg)

 

\# 连接到数据库（修改）

```dockerfile
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY 'root' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'root' WITH GRANT OPTION;

FLUSH PRIVILEGES;
```



#### centos7 配置python

```dockerfile
yum install openssl-devel bzip2-devel expat-devel gdbm-devel readline-devel sqlite-devel gcc gcc-c++ openssl-devel libffi-devel python-devel mariadb-devel

# 下载python 3.8
wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz

# 解压
 tar -zxvf Python-3.8.6.tgz -C /tmp
 cd /tmp/Python-3.8.6/
# 编译
./configure --prefix=/usr/local
# 清除
make
make altinstall
# 更改软连接
ln -s /usr/local/bin/python3.8 /usr/bin/python3
ln -s /usr/local/bin/pip3.8 /usr/bin/pip3

# 如果装错，如何删除软连接
rm -rf ./python3
```

# Ubuntu 中配置docker

```bash
# 先卸载可能存在的旧版本
apt remove docker docker-engine docker-ce docker.io
```

```bash
# 更新apt包索引
apt update
```

```bash
# 安装以下包以使apt可以通过HTTPS使用存储库
apt install -y apt-transport-https ca-certificates curl software-properties-common
```

```bash
使用阿里云的docker源进行安装的话要添加对应源的密钥，docker官方的要对应官方的密钥，选择不同的源安装密钥不一样的
```

```bash
# 镜像源
-- 阿里云
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -

-- 国外官方
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

-- ubuntu中软件源的位置
ubuntu的系统源文件位置位于/etc/apt/sources.list

add-apt-repository "deb [arch=arm64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
----------------------------------------------------------
>>> 其中$(lsb_release -cs)是个系统函数，可以自动获取当前ubuntu系统的版本，arm64对应的cpu的版本，我的主板是arm的，如果是电脑或者服务器注意是安装 amd64的，需要把arm64替换成amd64
同理第四步，添加docker镜像源的时候，网址里面的域名也要看选择哪个源而变化的，上面举例的是docker官方的
也可以使用阿里的源添加进去
https://developer.aliyun.com/mirror/docker-ce
----------------------------------------------------------

# 再次索引
apt update

# 安装最新版
apt install  docker-ce

# 验证docker
systemctl status docker

# 向source.list中添加软件源(可选)
sudo add-apt-repository "deb [arch=amd64] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu $(lsb_release -cs) stable
```

