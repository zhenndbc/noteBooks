```bash
# 永久更换镜像源地址
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple/
```

```bash
# 暂时更换
pip3 install pygame -i http://mirrors.aliyun.com/pypi/simple/
```

```bash
# 镜像地址大全
阿里云
http://mirrors.aliyun.com/pypi/simple/

中国科技大学
https://pypi.mirrors.ustc.edu.cn/simple/

豆瓣(douban)
http://pypi.douban.com/simple/

清华大学
https://pypi.tuna.tsinghua.edu.cn/simple/

中国科学技术大学
http://pypi.mirrors.ustc.edu.cn/simple/

# 安装指令
pip install keras==2.3.1 -i http://pypi.douban.com/simple/
//2.3.1为所需版本号，尾部url为对应镜像源地址

```

