# 使用 `Wails` 开发

## 换源

```shell
# 淘宝源地址
npm config set registry https://registry.npm.taobao.org
```

---

## 下载 `Wails`

`安装`

```shell
go install github.com/wailsapp/wails/v2/cmd/wails@latest
```

`检测依赖`

```shell
wails doctor
```

`使用模板`

```shell
# 基于 js 的
wails init -n myproject -t svelte

# 基于 ts 的
wails init -n myproject -t svelte-ts

# vanilla
wails init -n myproject -t vanilla

# vue
wails init -n myproject -t vue

# react
wails init -n myproject -t react
```

`项目结构`

```markdown
.
├── build/
│   ├── appicon.png
│   ├── darwin/
│   └── windows/
├── frontend/
├── go.mod
├── go.sum
├── main.go
└── wails.json
```

- `/main.go` - 主应用
- `/frontend/` - 前端项目文件
- `/build/` - 项目构建目录
- `/build/appicon.png` - 应用程序图标
- `/build/darwin/` - Mac 特定的项目文件
- `/build/windows/` - Windows 特定的项目文件
- `/wails.json` - 项目配置
- `/go.mod` - Go module 文件 -> 可以修改 changme 部分
- `/go.sum` - Go module 校验文件

---

## 开发者模式

开发 `wails dev`

```markdown
1. 使用 vite 监控文件变化
2. 使用 启动网络程序 localhost:34115 开发
3. 使用 JS 代码绑定 Go 到前端
```

---

## Wails 指令

```shell
# 生成项目
wails init -n proName -t vue/react/sevlte

# 编译项目
wails build

# 开发模式
wails dev
```

