# Python学习中遇到的小问题

- 关于异常

  > ![image-20220613012309993](C:\Users\lcw\AppData\Roaming\Typora\typora-user-images\image-20220613012309993.png)

- ```python
  # 异常检测
  """
  1. try...except...else...finally
  2. try...else
  3. try...except
  4. try...finally
  """
  --> 异常检测，若捕获异常则抛出。
  ```

- ```python
  # -*- coding:UTF-8 -*-
  try:
      # 导入需要加载,但是没有安装的模块
      import pyqt5
  except ImportError as error:  # 检测(模块未安装)异常
      # 导入用于检测的标准库
      from re import search  # search 用来查找相关的字符串
      from os import system, path  # system 用于调动命令窗口,或者bash进程 执行语句
      from sys import executable  # executable 用于获取当前环境下解释器的绝对路径
      
      # 代码实现
      """
      基于 pip install packname -i 镜像url
  		--使用 format 拼接代码
  		--引用清华源
  		--将异常转换为字符串类型, 作为正则表达式的索引文本
  		--使用表达式进行模式匹配 -->> "'(.*?)'"
  		--通过executable 获取当前解释器的路径,确保安装的路径没有错误
      """
      system('{} -m pip install --user {}  -i https://pypi.tuna.tsinghua.edu.cn/simple'
             .format(executable, search("'(.*?)'", str(error))[1]))
  ```

- ```python
  # 获取当前解释器的位置
  os.path.dirname(sys.executable)
  ```

---

![PyQt5的小项目](C:\Users\lcw\AppData\Roaming\Typora\typora-user-images\image-20220613150237508.png)

## 基于PyQt5的小项目

1. 初始化

   > ```python
   > 
   > ```
   >
   > 