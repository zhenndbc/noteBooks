```bash
# 软件开源地址
https://github.com/hanxinhao000/ZeroTermux


# 连接服务器
pkg install -y curl ; bash -c "$(curl -L gitee.com/mo2/linux/raw/2/2)"


--->>> 问你是否安装<wiptail>-----然后获取手机存储权限

# 克隆项目地址---->>> (自动安装)
git clone this repo to /data/data/com.termux/files/home/.local/share/tmoe-linux/git?

# 手动编辑当前的main源
apt edit-sources

# 配置其他的main源地址
cd /data/data/com.termux/files/usr/etc/apt/sources.list.d ; nano ./*

--[[ 当前的mian源地址
#deb https://grimler.se/termux-packages-24/ stable main
deb https://mirrors.bfsu.edu.cn/termux/apt/termux-main stable main
#deb https://packages.termux.org/apt/termux-x11 x11 main
deb https://mirrors.bfsu.edu.cn/termux/apt/termux-x11 x11 main
]]

# 主题121 配色111 字体原样

# 启动工具tmoe
debian-i



# 初次使用
pkg install x11-repo

# 软件包自行更新
pkg upgrade

# 安装与查找软件包
pkg install <package>
pkg search <query>

# 图片包的位置
/root/图片/ubuntukylin-wallpapers
/root/Pictures/ubuntukylin-wallpapers

# 卸载dock栏
apt purge -y  plank

```

```markdown
--------------------
列出当前已安装的容器:tmoe ls
启动默认proot容器:tmoe p, if you are using tmoe-zsh, just type t p
启动code_arm64的完整命令为 tmoe proot code  arm64
您若未启用跨架构运行的功能，则可去除arm64参数。

精简命令为  tmoe p code  a
您若使用了tmoe-zsh配置的环境，则可将其进一步简化为 t p code  a
同时启动code_arm64容器+tigervnc server的命令为  tmoe p code  a v
同时启动code_arm64容器+x11vnc server的命令为  tmoe p code  a x11
同时启动code_arm64容器+XSDL/VcXsrv的命令为  tmoe p code  a xs

容器的默认登录SHELL为zsh,若您需要使用其它shell，则请使用以下命令
临时使用bash作为登录SHELL：  tmoe p code  a bash
临时使用ash作为登录SHELL：  tmoe p code  a ash

注:最后一个参数可以执行容器的指定命令。若为长命令，则请使 用引号将最后一个参数包围起来。
例如：  tmoe p code  a "ls -lah /"
最后一个参数也可以执行本地脚本，如果当前目录下存在hello.rb文件，那么您需要执行 tmoe p code  a "./hello.rb"
而不是  tmoe p code  a "hello.rb"
注：本地脚本或二进制文件的优先级要高于容器内部文件，详见“ 环境变量与登录项管理”选项的README。
Example(示例):
cat >hello.rb <<-'RUBY'
#!/usr/bin/env ruby
50.times{p "hello"}
RUBY
tmoe p code  a ./hello.rb '
if [[ ! $(command -v ruby) ]];then
    sudo apt update
    sudo apt install -y ruby || sudo apk add ruby || sudo pacman -Sy ruby || sudo dnf install ruby
    ./hello.rb
fi
'

v1.4539.0新版功能：
您现在可以使用ln参数来创建容器内部文件路径的软链接到本地路径。
例如：
tmoe p code  a ln /tmp
软链接code_arm64容器内部的/tmp到本地的 "container_link_tmp_随机数值"
您在当前路径下执行cd container_link_tmp_*后，将进入容器所 在路径的/tmp目录。
--------------------

额外拓展功能
（补全功能）说明
shell环境要求：zsh
The completion function exists separately as a zsh plugin, so bash and other shells are not supported.
仅支持zsh,不支持bash

tmoe-zsh会自动加载tmoe补全插件，其他插件管理器需要手动加载。
Tmoe-zsh manager is based on zinit.
zinit插件管理器配置补全插件：
[[ $(grep -E '^[^#]*zinit.*completion/_tmoe' /data/data/com.termux/files/home/.zshrc) ]] || sed -i '$ a\zinit ice lucid wait=1 as"completion" && zinit snippet /data/data/com.termux/files/home/.local/share/tmoe-linux/git/share/old-version/share/completion/_tmoe' /data/data/com.termux/files/home/.zshrc

至于其他插件管理器，例如oh-my-zsh，因开发者并未进行测试， 故请自行加载插件。
TIPS OF TMOE COMPLETION PLUGIN
  1.在TMOE-ZSH配置的环境下,输t,按下空格,再按下TAB键⇄进行补全，输/进行连续补全，在该模式下支持搜索发行版名称。
  2.在其他插件管理器配置的环境下，输入tmoe,不按回车,按下TAB键⇄进行补全.
  Type tmoe, then don't press enter, press Tab⇄ key to complete.
```

```bash
# 删除镜像文件
rm /root/sd/Download/backup/rootfs/archlinux-latest_amd64-rootfs.tar.xz
```
