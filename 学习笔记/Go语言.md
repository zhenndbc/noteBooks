# Go语言

## 并发基础之 Goroutine和channel

> 1. 接收者  `v := <- ch`
> 2. 发送者 `ch <- 'Someting'`

```go
func main() {
	ch := make(chan string)

	go func() {
		fmt.Println("空安")
		ch <- fmt.Sprintf("finished")
	}()

	fmt.Println("我是 main goroutine")

	v := <-ch
	fmt.Println("接收到参数是: " + v)
}
```

---

## channel分类

> 提倡通过通信来共享内存，而不是通过共享内存来通信。

1. 无缓冲

   > 容量是 0 不能存储任何数据
   >
   > 只起到传输数据的作用
   >
   > 同步 channel

2. 有缓冲

   > 通过初始化 make 设置 容量
   >
   > `cacheCh := make(chan string, 5)`

3. 单向channel

   > 限制只能接收，或者只能发送
   >
   > `onlySend := make(chan<- string)`
   >
   > `onlyReceive := make(<-chan string)`

---

`select + channel`

```go
func main() {
	// 声明三个存放结果的channel
	firstCh := make(chan string)
	secondCh := make(chan string)
	threeCh := make(chan string)

	// 同时开启三个 goroutine 下载
	go func() {
		firstCh <- downloadFile("firstCh")
	}()

	go func() {
		secondCh <- downloadFile("secondCh")
	}()

	go func() {
		threeCh <- downloadFile("threeCh")
	}()

	// 通过 select 哪个 goroutine 先完成便使用对应数据
	select {
	case filePath := <-firstCh:
		fmt.Println(filePath)
	case filePath := <-secondCh:
		fmt.Println(filePath)
	case filePath := <-threeCh:
		fmt.Println(filePath)
	}
}

func downloadFile(chanName string) string {
	// 模拟下载文件
	time.Sleep(time.Second * 3)
	return chanName + ":filePath"
}
```

---

## RPC

> 远程过程调用
>
> => 对应 本地过程调用

---

```python
def demo(a: int, b: int):
    info = a + b
    return info

result = demo(1, 2)
print(result)
```

1. 将 1 2 入栈
2. 进入demo函数，取出 1 2 赋值给 临时变量info
3. 栈底弹出元素 info 赋值给全局变量 result 结束调用

---
## 数据库操作
```go
package core

import (
	"crypto/md5"
	"encoding/hex"
	"errors"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Image struct {
	ImgID      string `gorm:"primaryKey"`
	ImgName    string
	ImgContent []byte
}

var (
	db     *gorm.DB
	ErrMap = map[int]error{
		400: errors.New("图片已存在"),
		401: errors.New("创建图片失败"),
		402: errors.New("存储图片失败"),
		403: errors.New("查询图片失败"),
		404: errors.New("图片不存在"),
		405: errors.New("删除图片失败"),
	}
)

func init() {
	initDB()
}

func initDB() {
	var err error
	db, err = gorm.Open(sqlite.Open("images.db"), &gorm.Config{})
	if err != nil {
		log.Fatal("failed to connect database:", err)
	}

	err = db.AutoMigrate(&Image{})
	if err != nil {
		log.Fatal("failed to migrate database:", err)
	}

	db.Callback().Query().Before("gorm:query").Register("disable_raise_record_not_found", func(d *gorm.DB) {
		d.Statement.RaiseErrorOnNotFound = false
	})
}

func md5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func QueryByName(imgName string) (*Image, error) {
	imgID := md5Hash(imgName)

	img, ok := Query(imgID)
	if ok {
		return nil, ErrMap[403]
	}

	return img, nil
}

func Query(imgID string) (img *Image, exist bool) {
	img = new(Image)

	err := db.First(
		img,
		"img_id = ?", imgID,
	).Error

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			exist = false
		} else {
			log.Error(err)
			exist = true
		}
	} else {
		exist = true
	}

	return img, exist
}

func GetAllImages() ([]Image, error) {
	var images []Image
	if err := db.Find(&images).Error; err != nil {
		return nil, ErrMap[403]
	}
	return images, nil
}

func CreateImage(imgName string, imgContent []byte) (string, error) {
	imgID := md5Hash(imgName)
	newImage := Image{
		ImgID:      imgID,
		ImgName:    imgName,
		ImgContent: imgContent,
	}

	if _, ok := Query(imgID); ok {
		return "", ErrMap[400]
	}

	if err := db.Create(&newImage).Error; err != nil {
		return "", ErrMap[401]
	}

	return imgID, nil
}

func UpdateImage(imgID string, newContent []byte) error {
	img, ok := Query(imgID)
	if !ok {
		return ErrMap[404]
	}

	img.ImgContent = newContent
	if err := db.Save(&img).Error; err != nil {
		return ErrMap[404]
	}

	return nil
}

func DeleteImage(imgID string) error {
	if err := db.Delete(&Image{}, "img_id = ?", imgID).Error; err != nil {
		return ErrMap[405]
	}
	return nil
}

```