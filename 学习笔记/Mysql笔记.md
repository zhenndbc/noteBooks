## 尚硅谷课程教学

```mysql
# 创建一张表
CREATE TABLE 表名
(
    列名 数据类型(数据长度)
)
```

![image-20220517202505019](C:\Users\kong\AppData\Roaming\Typora\typora-user-images\image-20220517202505019.png)

```markdown
CHAR和VARCHAR的区别：

    char 占固定长度，即使没有那么长也会占用。

    varchar则会用增加的一个字节位 来保存长度位，相对来说”abc“就只占4个字节了
ENUM 和 SET 的区别: 
	ENUM 类型的数据的值，必须是定义时枚举的值的其中之一，即单选，而 SET 类型的值则可以多选。
```

```mysql
# 查看表中的内容
SELECT * FROM 表名;

# 插入数据
INSERT INTO 表的名字(列名a,列名b,列名c) VALUES(值1,值2,值3);

# 模拟插入数据
INSERT INTO employee(id,name,phone) VALUES(01,'Tom',110110110);

INSERT INTO employee VALUES(02,'Jack',119119119);

INSERT INTO employee(id,name) VALUES(03,'Rose');
```

```markdown
有的数据需要用单引号括起来，
	比如 Tom、Jack、Rose 的名字，这是由于它们的数据类型是 CHAR 型。
	 此外 VARCHAR,TEXT,DATE,TIME,ENUM 等类型的数据也需要单引号修饰，
	  而 INT,FLOAT,DOUBLE 等则不需要。
```

```mysql
# 查询版本号
select version();

mysql --version
```

```mysql
# 修改编码格式

```

```mysql
---行(row)---列(column)---模式(schema)---数据库(database)
---结构化查询语言(Structured Query Language)
```

<h2>《书籍信息》连接</h2>

> 管理登录受到密切保护
>
> （因为对它的访问授予了创建表、删除整个数据库、更改登录和口令等完全的权限）
>
> 主机名(localhost)

```mysql
# 使用数据库(先打开数据库，才能读取其中的数据)
mysql> USE 数据库名;
Database changed

# 展示数据库中的表
mysql> SHOW TABLES;

# 显示表列--->> 返回字段、数据类型、是否允许NULL、键信息、默认值以及其他信息
mysql> SHOW COLUMNS FROM 表名;
---相当于---
mysql> DESCRIBE 表名; <------> mysql> DESC 表名;

# 显示创建 特定数据库或表 的MySQL语句
mysql> SHOW CREATE DATABASE 数据库名; 
mysql> SHOW CREATE TABLE 表;

# 显示所有数据库
mysql> SHOW DATABASES;

```

>   一、什么是自动增量？  某些表列需要唯一值。例如，订单编号、雇员ID或（如上面例子中所示的）顾客ID。在每个行添加到表中时，MySQL可以自动地为每个行分配下一个可用编号，不用在添加一行时手动分配唯一值（这样做必须记住最后一次使用的值） 。这个功能就是所谓的自动增量。如果需要它，则必须在用CREATE语句创建表时把它作为表定义的组成部分。我们将在第21章中介绍CREATE语句。

>   二、DESCRIBE语句  MySQL支持用DESCRIBE作为SHOW COLUMNS FROM的一种快捷方式。换句话说，DESCRIBE customers;是SHOW COLUMNS FROM customers;的一种快捷方式。

>   三、其余的SHOW指令
>
> SHOW COLUMNS FROM 表;
>
> SHOW STATUS; 用于显示广泛的服务器状态信息;
>
> SHOW CREATE DATABASE 和 SHOW CREATE TABLE，分别用来显示创建特定数据库或表的MySQL语句;
>
> SHOW ERRORS和SHOW WARNINGS，用来显示服务器错误或警告消息。

<h3>检索数据库</h3>

>为了使用SELECT检索表数据，必须至少给出两条信息——想选择什么，以及从什么地方选择。

```MYSQL
1.1 检索单个列
# 所需的列名在SELECT关键字之后给出，FROM关键字指出从其中检索数据的表名。
mysql> SELECT USER FROM USER;
--->> 此数据可能未被排序---但只要是相同的即可

# 注意事项
1. 结束SQL语句多条SQL语句必须以分号（；）分隔。
2. SQL语句不区分大小写,开发人员喜欢使用大写表示关键词，使用小写表示列和表名。
3. SQL语句的空格会被忽略，多行SQL易于阅读和调试。

1.2 检索多个列
# 使用相同的select，在SELECT关键字后给出多个列名，列名之间必须以逗号分隔，但是最后一个不用加。
mysql> SELECT USER,HOST FROM USER;
--->> 排列出来的顺序与查询顺序一致

>数据表示从上述输出可以看到，SQL语句一般返回原始的、无格式的数据。数据的格式化是一个表示问题，而不是一个检索问题。因此，表示（对齐和显示上面的价格值，用货币符号和逗号表示其金额）一般在显示该数据的应用程序中规定。一般很少使用实际检索出的原始数据（没有应用程序提供的格式）。

1.3 检索所有的列
mysql> SELECT * FROM USER;
--->> 检索自己不需要的东西，会降低程序性能。
	--->> 但是通配符（*）能检测到那些名字未知的列。
	
1.4 检索不同的行
mysql> SELECT DISTINCT 列名 FROM 表名;
mysql> SELECT DISTINCT USER FROM USER;
--->> 如果使用DISTINCT关键字，它必须直接放在列名的前面。
	--->> 只返回不同（唯一）的行，包括本身。
		--->> 不能部分使用，否则会返回所有的。
		--->> SELECT DISTINCT USER,HOST FROM USER;

1.5 限制结果
mysql> SELECT DISTINCT USER,HOST FROM USER LIMIT 3,2;
--->> LIMIT 5, 5指示MySQL返回从行5开始的5行。第一个数为开始位置，第二个数为要检索的行数。
mysql>  SELECT USER,HOST FROM USER LIMIT 5;
--->> LIMIT 5指示MySQL返回不多于5行。
mysql> SELECT USER,HOST FROM USER LIMIT 1 OFFSET 3;
--->> 在MYSQL5中LIMIT语法，从第3行开始取一行。

> 行0  检索出来的第一行为行0而不是行1。因此，LIMIT 1, 1将检索出第二行而不是第一行。
> 在行数不够时  LIMIT中指定要检索的行数为检索的最大行数。
	如果没有足够的行（例如，给出LIMIT 10, 5，但只有13行），MySQL将只返回它能返回的那么多行。

1.6 完全限定的列名
mysql> SELECT 表名.列名 FROM 表名.数据库名;
----->> 日后拓展
mysql> SELECT USER FROM mysql.user;

-------->>>>>>>>>>>> 2022/5/23 3:33 学完《MYSQL必知必会》第4章节
```

