## 前言
在前后端分离的项目开发过程中，如果后端同学能够提供一份清晰明了的接口文档，那么就能极大地提高大家的沟通效率和开发效率。那如何维护接口文档，历来都是令人头痛的，感觉很浪费精力，而且后续接口文档的维护也十分耗费精力。在很多年以前，也流行用word等工具写接口文档，这里面的问题很多，如格式不统一、后端人员消费精力大、文档的时效性也无法保障。
针对这类问题，最好是有一种方案能够既满足我们输出文档的需要又能随代码的变更自动更新，Swagger正是那种能帮我们解决接口文档问题的工具。
## Swagger介绍

Swagger是基于标准的 OpenAPI 规范进行设计的，本质是一种用于描述使用json表示的Restful Api的接口描述语言，只要照着这套规范去编写你的注解或通过扫描代码去生成注解，就能生成统一标准的接口文档和一系列 Swagger 工具。Swagger包括自动文档，代码生成和测试用例生成。
## 1.安装Swagger

```shell
go get -u github.com/swaggo/swag/cmd/swag
```
## 2.检查是否安装
```shell
swag -v
> swag version v1.8.4
```
## 3.安装拓展
```shell
go get -u -v github.com/swaggo/gin-swagger
go get -u -v github.com/swaggo/files
go get -u -v github.com/alecthomas/template
```

## 使用
> gin-swagger
步骤：
1. 按照swagger要求给接口代码添加声明式注释。
2. 使用swag工具扫描代码自动生成api接口文档数据。
3. 使用gin-swagger渲染在线接口文档页面。

![[Pasted image 20231210191800.png]]

### 生成文档
```shell
%USERPROFILE%\go\bin\swag init
go run .
```