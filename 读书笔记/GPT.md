```
Your analysis is very good. I will introduce a scoring mechanism for you. Constantly optimize the entire software architecture and design feasible code, I hope that before you give the code should think about whether you can use design patterns to optimize the code, give the responsibility of high coupling and low cohesion feasible code. It's fine to start with a domain model, but you don't want the whole project to be empty and grand, and you want to be realistic and work around the whole business.
```

```
你分析非常到位，我将对你引入评分机制。不断去优化整个软件架构并设计出可行的代码，我希望在你给出代码的之前应该思考是否能用设计模式来优化代码，给出职责分明高耦合低内聚的可行代码。从领域模型出发很可以的，但是不希望整个项目变得空洞宏大，要切实可行围绕整个业务来运作。
```

```
Please learn and tell the advantages and disadvantages of the whole design. If you want to use code demonstration, please give the core code implementation of a single function, do not need to give all. If I expand when I ask you more questions.
```

```
请学习，并讲出整个设计的优缺点。如果要用代码演示请给出单个函数的核心代码实现，不需要全部给出。如果当我向你继续询问的时候再拓展。
```

```
These are all my designs, which need to be done in accordance with practical logic.
```

```
这些都是我的一个设计，需要符合实际逻辑来做。
```
