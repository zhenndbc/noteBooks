```jsx
import React, {useCallback, useState} from 'react';
import {Button, Layout, Menu} from 'antd';
import {
    CloudFilled,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    RobotOutlined,
    SettingFilled,
    UserOutlined
} from '@ant-design/icons';
import TaskTest from "./TaskTest.jsx";
import CodeEditor from "./CodeTest.jsx";
import LoginTest from "./LoginTest.jsx";
import ConfigTest from "./ConfigTest.jsx";
import "../../css/demo.css"


const {Header, Sider, Content} = Layout;

const ManageLayout = () => {
    const [collapsed, setCollapsed] = useState(false);
    const [selectedMenuItem, setSelectedMenuItem] = useState('1');

    const toggleCollapsed = useCallback(() => {
        setCollapsed(prevCollapsed => !prevCollapsed);
    }, []);

    const handleMenuClick = useCallback((e) => {
        setSelectedMenuItem(e.key);
    }, []);

    const renderContent = useCallback(() => {
        switch (selectedMenuItem) {
            case '1':
                return <TaskTest/>;
            case '2':
                return <CodeEditor/>;
            case '3':
                return <LoginTest/>;
            case '4':
                return <ConfigTest/>;
            default:
                return <div>Select a menu item</div>;
        }
    }, [selectedMenuItem]);

    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="demo-logo-vertical"/>
                <Menu
                    theme="dark"
                    mode="vertical"
                    defaultSelectedKeys={['1']}
                    onClick={handleMenuClick}
                    items={[
                        {
                            key: '1',
                            icon: <RobotOutlined/>,
                            label: '任务集',
                            onClick: () => setSelectedMenuItem('1'),
                        },
                        {
                            key: '2',
                            icon: <CloudFilled/>,
                            label: '编辑器',
                            onClick: () => setSelectedMenuItem('2'),
                        },
                        {
                            key: '3',
                            icon: <UserOutlined/>,
                            label: '登录',
                            onClick: () => setSelectedMenuItem('3'),
                        },
                        {
                            key: '4',
                            icon: <SettingFilled/>,
                            label: '配置',
                            onClick: () => setSelectedMenuItem('4'),
                        },
                    ]}
                />
            </Sider>
            <Layout>
                <Header style={{padding: 0}} className={'base-header-div'}>
                    <Button type="text" onClick={toggleCollapsed}>
                        {collapsed ? <MenuUnfoldOutlined/> : <MenuFoldOutlined/>}
                    </Button>
                    <img src='https://v2.jinrishici.com/one.svg' alt='loading...'/>
                </Header>
                <Content style={{margin: '24px 16px', padding: 24, minHeight: 280}}>
                    {renderContent()}
                </Content>
            </Layout>
        </Layout>
    );
};

export default ManageLayout;

```

```jsx
import { redirect } from 'react-router-dom'
```