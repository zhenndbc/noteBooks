## 介绍
独立开发者：Tim Nolet

开发者背景：年近四十，曾主修艺术史的荷兰web和IT工程师，爱好吉他和合成器，善玩Battlefield游戏。  

项目简介：Checkly是用于监测API接口状态和网站事务（例如点击登录、注册、购物等关键点）的SaaS软件。

开发初衷：Tim当初在一家德国公司负责IT，公司的测试不足，导致用户注册和销售流程常常出故障，于是萌生了自动化监测的想法。

开发过程：起初Tim低估了工作量，还用了一堆自己不熟悉的技术。之后从MongoDB切换到Postgres，从AWS Lamdba换回了常规的web开发技术。他边接项目边兼职开发，花了大约六个月发布。

## gorod
```go
func CreateIfNotExists(path string, isDir bool) error {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			if isDir {
				return os.MkdirAll(path, 0o755)
			}
			if err := os.MkdirAll(filepath.Dir(path), 0o666); err != nil {
				return err
			}
			f, err := os.OpenFile(path, os.O_CREATE, 0o666)
			if err != nil {
				return err
			}
			_ = f.Close()
		}
	}
	return nil
}
```