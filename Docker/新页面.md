# 新页面

```go
package service

// 系统
type system struct{}

// 题库
type QuestionBank struct {
  // 题目 ID (标题哈希后产生)
  QuestionID string

  // 题目类型 (单选 | 多选)
  QuestionType string

  // 题目内容 (标题 | 内容)
  QuestionInfo struct {
    QuestionTitle   string // 问题-标题
    QuestionContent string // 问题-内容
    QuestionAnswer  string // 问题-答案
  }
}

// 考试
type Exam struct {
  // 考试ID
  ExamID string
  // 考试时间信息
  ExamInfo struct {
    CourseName  string // 课程名称
    ExamName    string // 考试名称
    LengthAT    string // 考试时长
    ExamTeacher string // 创建老师
  }
  // 考卷
  ExaminationPaper any
}

// 课程
type Course struct {
  // 课程ID
  CourseID string
  // 课程信息
  CourseInfo struct {
    CourseName  string // 课程名
    CourseTitle string // 课程名
  }
}

// 班级
type Class struct {
  // 班级ID
  ClassID string
  // 班级信息
  ClassInfo struct {
    ClassName   string   // 班级名
    StudentList []string // 学生列表
    TeacherName string   // 老师名
  }
}

// 用户
type User struct {
  // 用户ID
  UserID string
  // 用户信息
  UserInfo struct {
    UserName string // 用户名 (设置唯一 | 由此判断)
    UserRole string // 用户角色
  }
}

```

```go
package main

import (
  "online-exam/pkg/class"
  "online-exam/pkg/course"
  "online-exam/pkg/exam"
  "online-exam/pkg/user"
)

type (
  // UserManager 定义接口：用户管理 (角色 => [教师 | 学生])
  UserManager interface {
    // CreateUser 创建用户
    CreateUser(user user.Info)
    // UpdateUser 更新用户信息
    UpdateUser(userID string, userInfo user.Info)
    // DeleteUser 删除用户
    DeleteUser(userID string)
    // GetUserByID 通过 ID 获取用户
    GetUserByID(userID string) *user.User
  }
  // ExamManager 定义接口：考试管理
  ExamManager interface {
    // CreateExam 创建考试
    CreateExam(exam exam.Exam)
    // ViewExamInfo 查看考试信息
    ViewExamInfo(examID int) exam.Exam
    // EditExam 编辑考试
    EditExam(examID int, updatedExam exam.Exam)
    // DeleteExam 删除考试
    DeleteExam(examID int)
  }
  // QuestionManager 定义接口：题库管理
  QuestionManager interface {
    // AddQuestion 增加题库
    AddQuestion(examID int, question exam.Question)
    // DeleteQuestion 删除题库
    DeleteQuestion(examID, questionID int)
  }
  // GradeManager 定义接口：成绩管理
  GradeManager interface {
    // SubmitGrade 提交成绩
    SubmitGrade(examID int, userID int, grade float64)
    // ViewGrade 查看成绩
    ViewGrade(examID int, userID int) float64
    // CollectExam 收藏考卷(考试中心)
    CollectExam(examId int)
  }
  // CourseManager 定义接口：课程管理
  CourseManager interface {
    // AddCourse 增加课程
    AddCourse(course course.Course)
    // UpdateCourse 修改课程
    UpdateCourse(courseID int, updatedCourse course.Course)
    // DeleteCourse 删除课程
    DeleteCourse(courseID int)
    // GetCourseByID 查询课程
    GetCourseByID(courseID int) *course.Course
  }
  // ClassManager 定义接口：班级管理
  ClassManager interface {
    // AddClass 向管理器中添加一个新班级。
    AddClass(class class.Class)
    // UpdateClass 更新已存在班级的信息。
    // 参数包括需要更新的班级的ID和新的班级信息。
    UpdateClass(classID int, updatedClass class.Class)
    // DeleteClass 根据班级ID删除一个班级。
    DeleteClass(classID int)
    // GetClassByID 根据班级ID获取班级信息。
    // 如果未找到，返回一个空班级。
    GetClassByID(classID int) *class.Class
  }
  // ExamCenter 定义接口：考试中心
  ExamCenter interface {
    StartExam(userID int, examID int)
    SubmitExam(userID int, examID int, answers map[int]string)
    GetExamResult(userID int, examID int) *user.ExamHistory
  }
)


```

***

下面是一个docker启动redis的命令，它会将redis部署到一个容器中，并使用端口映射将容器内的端口映射到主机上的指定端口，同时会将redis数据目录映射到主机上的一个数据目录，并使容器内的redis程序总是重启。

```go
docker run -d --name redis \
  -p 6379:6379 \
  -v /path/to/redis/data:/redis/data \
  --restart always \
  --name redis \
  --hostname redis \
  --network=host \
  redis
```

解释：

-   `-d` 选项表示启动一个后台进程。
-   `--name` 选项指定容器的名称，在此处为redis。
-   `-p 6379:6379` 选项指定容器内的端口映射，将容器内的端口6379映射到主机上的端口6379。
-   `-v /path/to/redis/data:/redis/data` 选项指定将容器内的redis数据目录映射到主机上的/redis/data目录。
-   `--restart always` 选项表示容器内的redis程序总是重启。
-   `--hostname redis` 选项将容器的主机名设置为redis。
-   `--network=host` 选项将容器设置为与主机网络同一网络。

如果你使用的是最新的Docker版本，也可以使用更简单的方式启动redis，只需使用以下命令即可：

```go
docker run --name redis \
  -p 6379:6379 \
  -v /path/to/redis/data:/redis/data \
  redis
```

这将自动启动redis，并将端口映射到6379，并将数据目录映射到/redis/data。

***

```go
func main() {
  // 使用函数式配置构建 AutoTask 结构体
  at := NewAutoTask(Option{}).
    WithImage("mysql:latest").
    WithNetwork("online_MicroService").
    WithContainerName("api-mysql").
    WithStaticIp("172.20.0.10").
    WithSubnet("172.20.0.0/16", "172.20.0.1").
    WithEnv([]string{"MYSQL_ROOT_PASSWORD=root", "MYSQL_PORT=3306"}).
    WithPortMappings(map[string]string{
      "3306/tcp": "3306",
    })

  if at.ContainerExists(at.opt.ContainerName) {
    // 运行 AutoTask
    at.Run()
  }

  // 用户服务
  userAPI := NewAutoTask(Option{}).
    WithImage("service-user:latest").
    WithContainerName("api-user").
    WithNetwork("online_MicroService").
    WithStaticIp("172.20.0.21").
    WithSubnet("172.20.0.0/16", "172.20.0.1").
    WithPortMappings(map[string]string{
      "8000/tcp": "8001",
      "9000/tcp": "9001",
    }).
    WithVolumeMappings([]VolumeMapping{
      {
        Source:   "/home/skong/manage/app/user/configs/",
        Target:   "/data/conf",
        ReadOnly: false, // 根据需要设置为只读或读写
      },
    })

  if userAPI.ContainerExists(userAPI.opt.ContainerName) {
    userAPI.Run()
  }
}
```

***

```go
{
  "editor.detectIndentation": false,
  "editor.tabSize": 4,
  "editor.formatOnPaste": true,
  "editor.formatOnSave": true,
  "go.toolsEnvVars": {
    "GOPATH": "/home/skong/go"
  },
  "files.exclude": {
    "**/laoma": true,
    "helloworld/**": true, // 藏起来同级的
    // "**/.vscode": true,
    "**/.idea": true,
    "**/learn-gin": true
  }
}

```

***

```go
判断请求来源和token：
首先，您可以创建一个中间件函数，用于检查请求的来源和 token 信息。在这个中间件中，您可以使用 c.Request.Host 检查请求的来源端口，并使用 c.GetHeader("Authorization") 获取请求头中的 token。如果条件不满足，可以使用 c.AbortWithStatusJSON 返回无权访问的响应。

心跳机制和配置文件读取：
您可以创建一个后台 goroutine，定期读取本地的 YAML 配置文件，其中包含微服务的地址信息。使用定时器来实现心跳机制，定期检查这些服务是否存活。如果服务不存活，可以采取适当的措施，如重新启动服务或记录日志。

代理请求到服务：
使用 Gin 的路由和中间件功能，您可以创建一个中间件来代理指定范围端口的所有请求，并将其转发到配置文件中的服务。这需要使用代理服务器功能，例如反向代理，将请求路由到正确的微服务。

高协程池和日志监控：
您可以创建一个协程池，根据配置文件中的服务数量，监控其日志。对于每个服务，创建一个协程来处理其日志，然后将日志消息放入队列中。使用 WebSocket 服务来统一返回给前端。您可以使用 Go 的 gorilla/websocket 包来实现 WebSocket 服务。
```

***

`请求来源 和 权限中间件`

```go
package main

import (
    "github.com/gin-gonic/gin"
)

func main() {
    router := gin.Default()

    // 创建一个路由分组，将中间件注册到该分组
    apiGroup := router.Group("/api/v1")
    {
        // 注册请求来源和 token 验证的中间件
        apiGroup.Use(checkSourceAndTokenMiddleware)
        
        // 添加不同的路由规则
        apiGroup.GET("/user", userHandler)
        apiGroup.GET("/exam", examHandler)
    }

    // 启动 Gin 服务器
    router.Run(":8080")
}

// 中间件函数：检查请求来源和 token
func checkSourceAndTokenMiddleware(c *gin.Context) {
    // 获取请求来源端口
    sourcePort := c.Request.Host
    // 获取请求头中的 Authorization 字段
    token := c.GetHeader("Authorization")

    // 根据您的规则进行判断，示例中检查了 sourcePort 和 token
    if sourcePort != "8080" || token == "" {
        c.JSON(403, gin.H{"message": "无权访问"})
        c.Abort() // 中止请求
        return
    }

    // 如果通过验证，继续处理请求
    c.Next()
}

func userHandler(c *gin.Context) {
    c.JSON(200, gin.H{"message": "用户接口"})
}

func examHandler(c *gin.Context) {
    c.JSON(200, gin.H{"message": "考试接口"})
}

```

***

`心跳机制`

```go
package main

import (
    "github.com/gin-gonic/gin"
    "gopkg.in/yaml.v2"
    "io/ioutil"
    "net/http"
    "time"
)

// 结构体定义用于解析 YAML 配置文件
type ServerConfig struct {
    Server1 struct {
        UserService struct {
            Port int    `yaml:"port"`
            Host string `yaml:"host"`
        } `yaml:"user-service"`
        ExamServer struct {
            Port int    `yaml:"port"`
            Host string `yaml:"host"`
        } `yaml:"exam-server"`
    } `yaml:"server1"`
}

// 用于存储服务可用性状态的全局变量
var serviceStatus = map[string]bool{
    "user-service": false,
    "exam-server":  false,
}

func main() {
    router := gin.Default()

    // 创建一个路由分组，将中间件注册到该分组
    apiGroup := router.Group("/api/v1")
    {
        // 注册心跳和配置文件读取中间件
        apiGroup.Use(heartbeatAndConfigMiddleware)
        
        // 添加不同的路由规则
        apiGroup.GET("/user", userHandler)
        apiGroup.GET("/exam", examHandler)
    }

    // 启动 Gin 服务器
    router.Run(":8080")
}

// 中间件函数：心跳和配置文件读取
func heartbeatAndConfigMiddleware(c *gin.Context) {
    // 在这里读取配置文件
    config, err := readConfigFile("config.yaml")
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"message": "无法读取配置文件"})
        c.Abort() // 中止请求
        return
    }

    // 定期检查服务的可用性
    go checkServiceAvailability(config)

    // 如果通过验证，继续处理请求
    c.Next()
}

// 读取配置文件的函数
func readConfigFile(filename string) (*ServerConfig, error) {
    data, err := ioutil.ReadFile(filename)
    if err != nil {
        return nil, err
    }

    var config ServerConfig
    err = yaml.Unmarshal(data, &config)
    if err != nil {
        return nil, err
    }

    return &config, nil
}

// 定期检查服务的可用性
func checkServiceAvailability(config *ServerConfig) {
    for {
        // 根据配置文件中的服务地址和端口检查服务是否存活
        checkServerStatus(config.Server1.UserService.Host, config.Server1.UserService.Port, "user-service")
        checkServerStatus(config.Server1.ExamServer.Host, config.Server1.ExamServer.Port, "exam-server")

        // 暂停一段时间再次检查
        time.Sleep(5 * time.Minute)
    }
}

// 检查服务可用性的函数
func checkServerStatus(host string, port int, serviceName string) {
    // 这里可以实现检查服务是否存活的逻辑，例如发送HTTP请求或其他检查方法
    // 如果服务存活，将服务的状态更新为 true
    // 如果服务不存活，将服务的状态更新为 false
    // 示例中只是模拟了一种简单的情况
    serviceStatus[serviceName] = true // 假设服务存活
}

func userHandler(c *gin.Context) {
    if !serviceStatus["user-service"] {
        c.JSON(http.StatusInternalServerError, gin.H{"message": "用户服务不可用"})
        return
    }
    c.JSON(200, gin.H{"message": "用户接口"})
}

func examHandler(c *gin.Context) {
    if !serviceStatus["exam-server"] {
        c.JSON(http.StatusInternalServerError, gin.H{"message": "考试服务不可用"})
        return
    }
    c.JSON(200, gin.H{"message": "考试接口"})
}

```

***

`代理服务器`

```go
package main

import (
    "github.com/gin-contrib/reverseproxy"
    "github.com/gin-gonic/gin"
    "net/http"
)

func main() {
    router := gin.Default()

    // 创建一个路由分组，将中间件注册到该分组
    apiGroup := router.Group("/api/v1")
    {
        // 注册代理中间件
        apiGroup.Use(reverseProxyMiddleware)
    }

    // 启动 Gin 服务器
    router.Run(":8080")
}

// 中间件函数：代理请求到服务
func reverseProxyMiddleware(c *gin.Context) {
    // 创建反向代理器
    proxy := reverseproxy.NewReverseProxy(
        reverseproxy.NewMultipleHostsProxyTargets(map[string]reverseproxy.Target{
            "user-service": {
                URL: "http://172.0.0.5:8080", // 配置文件中的用户服务地址
            },
            "exam-server": {
                URL: "http://172.0.0.3:8080", // 配置文件中的考试服务地址
            },
        }),
    )

    // 使用反向代理器处理请求
    proxy.ServeHTTP(c.Writer, c.Request)
}


```

***

`协程池访问控制`

```go
import (
    "github.com/gin-gonic/gin"
    "log"
    "net/http"
    "sync"
    "time"
)

// 日志记录器
var logger *log.Logger

// 创建一个有限的协程池
var workerPool chan struct{}

// 初始化日志记录器和协程池
func init() {
    logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)
    workerPool = make(chan struct{}, 10) // 设置协程池的大小
}

func main() {
    router := gin.Default()

    // 创建一个路由分组，将中间件注册到该分组
    apiGroup := router.Group("/api/v1")
    {
        // 注册协程池和日志监控中间件
        apiGroup.Use(workerPoolMiddleware)
        apiGroup.Use(logMonitoringMiddleware)
    }

    // 启动 Gin 服务器
    router.Run(":8080")
}

// 中间件函数：协程池控制
func workerPoolMiddleware(c *gin.Context) {
    select {
    case workerPool <- struct{}{}:
        // 协程池未满，继续处理请求
        c.Next()
        <-workerPool // 释放协程池中的一个协程
    default:
        // 协程池已满，返回错误响应
        c.JSON(http.StatusServiceUnavailable, gin.H{"message": "服务器繁忙"})
    }
}

// 中间件函数：日志监控
func logMonitoringMiddleware(c *gin.Context) {
    // 创建一个等待组，用于等待协程完成
    var wg sync.WaitGroup

    // 模拟处理请求的协程
    wg.Add(1)
    go func() {
        defer wg.Done()
        start := time.Now()
        // 处理请求的业务逻辑
        // 这里只是一个示例，您可以根据实际情况扩展
        time.Sleep(100 * time.Millisecond)
        elapsed := time.Since(start)
        logger.Printf("处理请求耗时：%v", elapsed)
    }()

    // 继续处理请求
    c.Next()

    // 等待协程完成
    wg.Wait()
}

```

***

`websoket`

```go
package main

import (
    "github.com/gin-gonic/gin"
    "github.com/gorilla/websocket"
    "net/http"
)

var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}

func main() {
    router := gin.Default()

    // 创建一个路由分组，将 WebSocket 中间件注册到该分组
    apiGroup := router.Group("/api/v1")
    {
        // 注册 WebSocket 中间件
        apiGroup.GET("/ws", websocketHandler)
    }

    // 启动 Gin 服务器
    router.Run(":8080")
}

// WebSocket 中间件
func websocketHandler(c *gin.Context) {
    // 升级HTTP连接为WebSocket连接
    conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"message": "无法升级为WebSocket连接"})
        return
    }
    defer conn.Close()

    // 在这里处理WebSocket连接
    for {
        // 读取前端发送的消息
        messageType, p, err := conn.ReadMessage()
        if err != nil {
            return
        }

        // 处理接收到的消息，这里示例将消息原样发送回去
        if err := conn.WriteMessage(messageType, p); err != nil {
            return
        }

        // 在服务器端处理完消息后，向前端发送数据
        responseMessage := []byte("这是来自服务器的消息")
        if err := conn.WriteMessage(websocket.TextMessage, responseMessage); err != nil {
            return
        }
    }
}

```

***
