# grpc配置

`下载: protoc`

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@1.28.1
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@1.2.0

```

`下载: Kratos`

```bash
go install github.com/go-kratos/kratos/cmd/kratos/v2@latest

```

# 验证码服务

## 初始化 Kratos

```bash
# 生成一个
kratos new verifyCode

# 国内
kratos new verifyCode -r https://gitee.com/go-kratos/kratos-layout.git
```

```bash
🍺 Project creation succeeded verifyCode
💻 Use the following command to start the project 👇:

cd verifyCode
go get github.com/google/wire/cmd/wire@v0.5.0

go generate ./...

go build -o ./bin/ ./...

./bin/verifyCode -conf ./configs

```

## 使用 Protobuf 定义验证码生成接口

1.  定义 Protobuf 文件说明接口
2.  利用 protoc 基于 protobuf 生成必要代码
3.  将生成的代码整合到项目中
4.  完善业务逻辑

### 增加 proto 文件模板

命令 `kratos proto add` 添加 `.proto` 文件

```bash
kratos proto add api/verifyCode/verifyCode.proto
```

```bash
  // 1.校验邮箱是否正确
  pattern := `^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$`
  regexpPattern := regexp.MustCompile(pattern)
  if !regexpPattern.MatchString(req.Telephone) {
    return &pb.GetVerifyResp{
      Code:    1,
      Message: "电话格式错误",
    }, nil
  }

  // 2.通过验证码服务生成验证码(服务间通信，grpc)
  // 连接 grpc 服务器
  conn, err := grpc.DialInsecure(context.Background(),
    grpc.WithEndpoint("localhost:9000"),
  )
  if err != nil {
    return &pb.GetVerifyResp{
      Code:    1,
      Message: "验证码服务不可用",
    }, nil
  }

  defer func() {
    _ = conn.Close()
  }()

  // 2.2,发送获取服务器代码
  client := verifyCode.NewVerifyCodeClient(conn)
  reply, err := client.GetVerifyCode(context.Background(), &verifyCode.GetVerifyCodeRequest{
    Length: 6,
    Type:   1,
  })
  if err != nil {
    return &pb.GetVerifyResp{
      Code:    1,
      Message: "验证码获取失败",
    }, nil
  }

  return &pb.GetVerifyResp{
    Code:    0,
    Message: reply.Code,
    Data:    time.Now().Unix(),
  }, nil
```
