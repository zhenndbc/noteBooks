# 服务proto

## 用户管理

```go
// 用户基本信息管理
type UserInfo struct {
  UserID    string // 用户 ID
  Username  string // 用户名
  Password  string // 用户密码
  Email     string // 用户邮箱
  CreatedAt string // 创建时间
  UpdatedAt string // 更新时间
  // 用户认证
  UserAuth Auth
}

// 用户认证和授权管理
type Auth struct {
  Token      string
  ExpiresAt  string
  Permission string
}
```

```json
{
  "UserID": "",
  "Username": "",
  "Password": "",
  "Email": "",
  "CreatedAt": "",
  "UpdatedAt": "",
  "UserAuth": {
    "Token": "",
    "ExpiresAt": "",
    "Permission": ""
  }
}
```

```protocol&#x20;buffers
// 用户管理
message UserInfo{
    
    // 用户权限
    message Userauth {
        string Token = 1;
        string ExpiresAt = 2;
        string Permission = 3;
    }
    
    string UserID = 1;
    string Username = 2;
    string Password = 3;
    string Email = 4;
    string CreatedAt = 5;
    string UpdatedAt = 6;
    Userauth UserAuth = 7;
}
```
