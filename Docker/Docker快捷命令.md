

## 一、查询
`列出所有容器`
```shell
docker ps -a
```
`快捷删除容器`
```shell
docker rm -f $(docker ps -q -n=1)
```

## 二、构建
`构建容器`
```shell
docker build -t demo:latest .
```
```shell
docker-compose up -d
```

## 三、运行
`启动一个开发容器`
```shell
docker run \
	-it --restart=always \
	-p 30000:8080 \
	# -v /code/dev/data:/ \
	--name=demo-dev \
	termux/termux-docker
```

```shell
export GOROOT=/usr/local/go
export PATH=$GOROOT/bin:$PATH
```

## 通过 graphsql 查询 github 仓库
[Explorer - GitHub Docs](https://docs.github.com/en/graphql/overview/explorer)
```shell
{
  repository(name: "go-cqhttp", owner: "Mrs4s") {
    release(tagName: "v0.9.0") {
      releaseAssets(first: 40) {
        nodes {
          downloadUrl
          name
          size
        }
        totalCount
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
      }
    }
  }
}
```
```shell
{
  repository(name: "go-cqhttp", owner: "Mrs4s") {
    releases(first: 100, orderBy: {field: CREATED_AT, direction: DESC}) {
      nodes {
        tagName
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
    }
  }
}
```